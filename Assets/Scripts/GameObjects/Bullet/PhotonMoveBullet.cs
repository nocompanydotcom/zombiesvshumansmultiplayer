﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PhotonMoveBullet : MonoBehaviour
{

    // Velocidade.
    [SerializeField]
    private Vector2 speed = new Vector2(5, 5);
    [SerializeField]
    private Vector2 direction = new Vector2(1, 0);

    // Guarda o movimento.
    private Vector2 movement;
    private Rigidbody2D m_Body;

    private void Awake()
    {
        this.m_Body = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        if (IsBulletTurnedToLeft())
        {
            this.Flip();
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Movimentacao pela direcao.
        movement = new Vector2(
            direction.x * speed.x,
            direction.y * speed.y);
    }

    void FixedUpdate()
    {
        // Movimento do objeto.
        this.m_Body.velocity = movement;
    }

    public void Flip()
    {
        this.direction = new Vector2(direction.x * -1, direction.y);
    }

    private bool IsBulletTurnedToLeft()
    {
        return (this.transform.rotation.y > 0);
    }
}
