﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PhotonShotBullet : MonoBehaviour
{
    [SerializeField]
    private int damage = 1;
    [SerializeField]
    private AudioClip explosionSound;

    private PhotonView m_PhotonView;
    private int OwnerPhotonViewID;

    private void Awake()
    {
        this.m_PhotonView = this.GetComponent<PhotonView>();
    }

    //Note that the bullet will only hit a player if the player it's not the own owner of this bullet
    void OnTriggerEnter2D(Collider2D other)
    {
        string tag = other.gameObject.tag;
        if (tag.Equals("Player"))
        {
            PhotonCharacter character = other.gameObject.GetComponent<PhotonCharacter>();
            if (character == null)
                Debug.LogError("No 'CharacterNetwork' script associated with the Player");
            else
            {
                if (this.m_PhotonView.ownerId == character.GetOwnerId())
                {
                    return;
                }
                else
                {
                    if (this.m_PhotonView.isMine)
                    {
                        character.GetPhotonView().RPC("RpcTakeDamage", PhotonTargets.All, this.OwnerPhotonViewID, this.gameObject.tag, damage);
                    }
                }
            }
        }

        Explode();

    }

    private void Explode()
    {
        //INSERT EXPLOSIONS EFFECTS HERE
        /*if (GetComponent<AudioSource>() != null)
            GetComponent<AudioSource>().Play();*/
        if (GetComponent<AudioSource>().clip != null)
            AudioSource.PlayClipAtPoint(this.explosionSound, this.transform.position, 1.0f);

        if (this.m_PhotonView.isMine)
            PhotonNetwork.Destroy(this.gameObject);
    }

    public void SetOwnerPhotonViewID(int ID)
    {
        this.OwnerPhotonViewID = ID;
    }
}
