using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ItemsController
{

    private GameObject itemsParent;
    private GameObject bulletItemPrefab;
    private GameObject multipleBulletsItemPrefab;
    private GameObject lifeItemPrefab;
    private GameObject poisonItemPrefab;
    private GameObject freezeItemPrefab;
    private uint maxBulletItem;
    private uint maxMultipleBulletsItem;
    private uint maxLifeItem;
    private uint maxPoisonItem;
    private uint maxFreezeItem;

    private List<GameObject> bulletItems;
    private List<GameObject> multipleBulletsItems;
    private List<GameObject> lifeItems;
    private List<GameObject> poisonItems;
    private List<GameObject> freezeItems;

    private SpawnPointGenerator spawnPointGenerator;

    public ItemsController(GameObject itemsParent, GameObject bulletItemPrefab, GameObject multipleBulletsItemPrefab, GameObject lifeItemPrefab, GameObject poisonItemPrefab, GameObject freezeItemPrefab, uint maxBulletItem, uint maxMultipleBulletsItem, uint maxLifeItem, uint maxPoisonItem, uint maxFreezeItem)
    {
        this.spawnPointGenerator = MapController.instance.getSpawnPointGenerator();

        this.itemsParent = itemsParent;
        this.bulletItemPrefab = bulletItemPrefab;
        this.multipleBulletsItemPrefab = multipleBulletsItemPrefab;
        this.lifeItemPrefab = lifeItemPrefab;
        this.poisonItemPrefab = poisonItemPrefab;
        this.freezeItemPrefab = freezeItemPrefab;
        this.maxBulletItem = maxBulletItem;
        this.maxMultipleBulletsItem = maxMultipleBulletsItem;
        this.maxLifeItem = maxLifeItem;
        this.maxPoisonItem = maxPoisonItem;
        this.maxFreezeItem = maxFreezeItem;

        this.bulletItems = new List<GameObject>();
        this.multipleBulletsItems = new List<GameObject>();
        this.lifeItems = new List<GameObject>();
        this.poisonItems = new List<GameObject>();
        this.freezeItems = new List<GameObject>();
    }

    public void InitItems()
    {
        if (PhotonNetwork.room.TryGetCurrentGameMode() == GAME_MODE.RESTART)
        {
            UpdateItemsPositions();
        }
        else
        {
            InstantiateItems();
        }
    }

    private void InstantiateItems()
    {
        while (this.bulletItems.Count < this.maxBulletItem)
        {
            GameObject item = GenerateItem(this.bulletItemPrefab);
            this.bulletItems.Add(item);
        }

        while (this.multipleBulletsItems.Count < this.maxMultipleBulletsItem)
        {
            GameObject item = GenerateItem(this.multipleBulletsItemPrefab);
            this.multipleBulletsItems.Add(item);
        }

        while (this.lifeItems.Count < this.maxLifeItem)
        {
            GameObject item = GenerateItem(this.lifeItemPrefab);
            this.lifeItems.Add(item);
        }

        while (this.poisonItems.Count < this.maxPoisonItem)
        {
            GameObject item = GenerateItem(this.poisonItemPrefab);
            this.poisonItems.Add(item);
        }

        while (this.freezeItems.Count < this.maxFreezeItem)
        {
            GameObject item = GenerateItem(this.freezeItemPrefab);
            this.freezeItems.Add(item);
        }
    }

    private void UpdateItemsPositions()
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        this.bulletItems = new List<GameObject>(GameObject.FindGameObjectsWithTag("Item_SingleBullet"));
        this.multipleBulletsItems = new List<GameObject>(GameObject.FindGameObjectsWithTag("Item_MultiAmmo"));
        this.lifeItems = new List<GameObject>(GameObject.FindGameObjectsWithTag("Item_Life"));
        this.poisonItems = new List<GameObject>(GameObject.FindGameObjectsWithTag("Item_Poison"));
        this.freezeItems = new List<GameObject>(GameObject.FindGameObjectsWithTag("Item_Freeze"));

        foreach (GameObject item in this.bulletItems)
        {
            item.transform.position = MapController.instance.getRealPosition(this.spawnPointGenerator.generateItemSpawnPoint());
        }

        foreach (GameObject item in this.multipleBulletsItems)
        {
            item.transform.position = MapController.instance.getRealPosition(this.spawnPointGenerator.generateItemSpawnPoint());
        }

        foreach (GameObject item in this.lifeItems)
        {
			Vector2 point = this.spawnPointGenerator.generateItemSpawnPoint ();
            item.transform.position = MapController.instance.getRealPosition(point);

			/*bool hey = false;
			foreach (Vector2 p in MapController.instance.mapGenerator.getEmptyPoints()) {
				if (point.x.Equals (p.x) && point.y.Equals (p.y)) {
					Debug.Log ("ENCONTROUUUUU");
					hey = true;
				}
			}
			if(!hey)
				Debug.Log("Real " + item.transform.position + " Matrix " + point);*/
        }

        foreach (GameObject item in this.poisonItems)
        {
            item.transform.position = MapController.instance.getRealPosition(this.spawnPointGenerator.generateItemSpawnPoint());
        }

        foreach (GameObject item in this.freezeItems)
        {
            item.transform.position = MapController.instance.getRealPosition(this.spawnPointGenerator.generateItemSpawnPoint());
        }
    }

    private GameObject GenerateItem(GameObject prefab)
    {
        Vector2 position = MapController.instance.getRealPosition(this.spawnPointGenerator.generateItemSpawnPoint());
        GameObject item = PhotonNetwork.InstantiateSceneObject(prefab.gameObject.name, position, Quaternion.identity, 0, null);
        item.transform.parent = this.itemsParent.transform; //It's necessary to do an RPC if we want that all the players understand that this item has the this parent!
        return item;
    }

	public void SetSpawnPointGenerator(SpawnPointGenerator spawnPointGenerator){
		this.spawnPointGenerator = spawnPointGenerator;
	}

    /*private GameObject GenerateItem(GameObject prefab)
    {
        Vector2 position = MapController.instance.getRealPosition(this.spawnPointGenerator.generateItemSpawnPoint());
        //return MapController.instance.InstantiateItem(prefab, position);
        GameObject item = PhotonNetwork.Instantiate(prefab.gameObject.name, position, Quaternion.identity, 0);
        item.transform.parent = this.itemsParent.transform;
        return item;
    }*/
}
