﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PhotonItem : MonoBehaviour
{

    protected PhotonView m_PhotonView;
    protected bool IsInCollision;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        this.m_PhotonView = GetComponent<PhotonView>();
        this.spriteRenderer = this.GetComponent<SpriteRenderer>();
    }

    // Use this for initialization
    public virtual void Bonus(PhotonCharacter character)
    {
        Debug.Log("Insert BONUS!!");
    }

    public void Disappear()
    {
        this.IsInCollision = false;
        Hide(); //Make it invisible
        PlaySound(); //Play sound on all the players
        Respawn();
    }

    private void PlaySound()
    {
        if (GetComponent<AudioSource>() != null)
        {
            AudioSource audioSource = this.GetComponent<AudioSource>();
            AudioSource.PlayClipAtPoint(audioSource.clip, this.transform.position);
        }
    }

    public void Respawn()
    {
        if (!this.m_PhotonView.isMine) //Only the master/owner client can assign a new position to the item
            return;

        SpawnPointGenerator spg = MapController.instance.getSpawnPointGenerator();
        if (spg == null)
            Debug.LogError("'SpawnPointGenerator' of 'MapController' is null");
        else
        {
            spg.addEmptyPoint(MapController.instance.getMapFakePosition(this.transform.position));
            Vector2 position = MapController.instance.getRealPosition(spg.generateItemSpawnPoint());
            this.m_PhotonView.RPC("RpcShow", PhotonTargets.All, position);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (this.IsInCollision) //Just trigger once the collision
            return;

        if (other.tag.Equals("Player"))
        {
            PhotonCharacter cn = other.gameObject.GetComponent<PhotonCharacter>();
            if (cn == null)
                Debug.LogError("Player is not associated with 'CharacterNetwork' script");
            else
            {
                if (!cn.IsDead())
                {
                    this.IsInCollision = true;
                    this.Bonus(cn);
                    this.Disappear();
                }
            }
        }
    }

    private void Hide()
    {
        this.spriteRenderer.enabled = false;
    }

    [PunRPC]
    public void RpcShow(Vector2 position)
    {
        this.gameObject.transform.position = position;
        this.spriteRenderer.enabled = true;
    }
}
