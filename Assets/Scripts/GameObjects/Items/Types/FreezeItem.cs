﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeItem : PhotonItem 
{
	public override void Bonus(PhotonCharacter character)
	{
		character.FreezeMovement();
	}
}