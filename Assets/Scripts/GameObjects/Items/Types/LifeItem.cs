﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeItem : PhotonItem {

    public override void Bonus(PhotonCharacter character)
    {
        if (character.IsHuman())
            character.AddCharacterLife();

        else character.TransformCharacter();
    }
}
