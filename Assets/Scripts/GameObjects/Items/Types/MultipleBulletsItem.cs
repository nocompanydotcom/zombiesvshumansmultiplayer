﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleBulletsItem : PhotonItem
{
    private static uint numBulletsOfBonus = 3;

    public override void Bonus(PhotonCharacter character)
    {
        if(character.IsHuman())
        {
            character.AddBullets(numBulletsOfBonus);
        }
    }
}
