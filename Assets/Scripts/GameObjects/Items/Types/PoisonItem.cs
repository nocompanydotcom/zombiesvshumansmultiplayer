﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonItem : PhotonItem {

	public override void Bonus(PhotonCharacter character)
	{
        if (character.IsHuman())
            character.TransformCharacter();

        else character.AddCharacterLife();
	}
}
