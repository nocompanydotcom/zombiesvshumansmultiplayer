﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleBulletItem : PhotonItem
{
    private static uint numBulletsOfBonus = 1;

	public override void Bonus(PhotonCharacter character)
    {

        if (character.IsHuman())
        {
            character.AddBullets(numBulletsOfBonus);
        }
    }
}
