using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{

    //Fields for the map generation
    [SerializeField]
    private int squarePixelSize = 64; //Pixels used for each square of the map    
    [SerializeField]
    private GameObject floorPrefab;
    [SerializeField]
    private GameObject mainObjectPrefab;
    [SerializeField]
    private string floorSortingLayerName = "Floor";
    [SerializeField]
    private int mapWidth = 400;
    [SerializeField]
    private int mapHeight = 400;
    [SerializeField]
    [Range(0, 100)]
    private uint randomAlgorithmFillPercentage = 50;
    [SerializeField]
    private bool smoothMapAlgorithm;
    [SerializeField]
    private string seed = null;
    [SerializeField]
    private bool removeNoiseAlgorithm;
    [SerializeField]
    private uint minimumNoise = 2;
    [SerializeField]
    private bool connectRoomsAlgorithm;
    [SerializeField]
    private uint connectionDiameter = 2;

    [Header("Border layout")]
    [SerializeField]
    private Sprite borderFloorSprite;
    [SerializeField]
    private GameObject borderGameObject;
    [SerializeField]
    private GameObject borderCornerObject;

    [Header("Items")]
    //Fields for the items generation
    [SerializeField]
    private GameObject bulletItem;
    [SerializeField]
    private GameObject multipleBulletsItem;
    [SerializeField]
    private GameObject lifeItem;
    [SerializeField]
    private GameObject poisonItem;
    [SerializeField]
    private GameObject freezeItem;

    [SerializeField]
    private uint maxBulletItem = 5;
    [SerializeField]
    private uint maxMultipleBulletsItem = 5;
    [SerializeField]
    private uint maxLifeItem = 5;
    [SerializeField]
    private uint maxPoisonItem = 5;
    [SerializeField]
    private uint maxFreezeItem = 5;

    private float squareSize;
    private Vector2 gameObject2DGameObjectSize;

    public MapGenerator mapGenerator;
    private ItemsController itemsController;
    private SpawnPointGenerator spawnPointGenerator;
    private GameObject itemsHolder;

    public static MapController instance;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(this);

        SetUpGameProperties();
    }

    public void InitMap()
    {
        if (this.itemsHolder == null)
        {
            this.itemsHolder = new GameObject("items");
            this.itemsHolder.transform.parent = this.transform;
        }

        //------ Generate Map ------
        if (this.mapGenerator == null)
            this.mapGenerator = new MapGenerator(floorPrefab, mainObjectPrefab, floorSortingLayerName,
                                                mapWidth, mapHeight, randomAlgorithmFillPercentage, smoothMapAlgorithm,
                                                removeNoiseAlgorithm, minimumNoise, connectRoomsAlgorithm, connectionDiameter, borderFloorSprite, borderGameObject, borderCornerObject);
        this.mapGenerator.Start(this.seed);
        this.spawnPointGenerator = new SpawnPointGenerator(this.mapGenerator.getEmptyPoints());
        //------ Start Items controller ------
        if (PhotonNetwork.isMasterClient)
        {
            if (this.itemsController == null)
                this.itemsController = new ItemsController(this.itemsHolder, bulletItem, multipleBulletsItem, lifeItem, poisonItem, freezeItem, maxBulletItem, maxMultipleBulletsItem, maxLifeItem, maxPoisonItem, maxFreezeItem);
			this.itemsController.SetSpawnPointGenerator (this.spawnPointGenerator);
            itemsController.InitItems();
        }
    }

    /*
     * This function is used when is to change map. 
    */
    public void Clean()
    {
        if (this.mapGenerator.GetObstaclesParent() != null)
            Destroy(this.mapGenerator.GetObstaclesParent());

        if (PhotonNetwork.room.GetCurrentGameMode() == GAME_MODE.RESTART) //If it's to restart the game just ignore the rest
            return;

        if (this.itemsHolder != null)
            Destroy(this.itemsHolder);

        if (this.mapGenerator == null) //No point to compare anything more if mapGenerator is null
            return;

        if (this.mapGenerator.GetBordersParent() != null)
            Destroy(this.mapGenerator.GetBordersParent());

        if (this.mapGenerator.GetFloorObject() != null)
            Destroy(this.mapGenerator.GetFloorObject());
    }

    public GameObject FakeInstantiateObject(GameObject item, Vector2 position, Transform parent)
    {
        item.transform.position = position;
        item.transform.parent = parent;
        BoxCollider2D collider = item.GetComponent<BoxCollider2D>();
        if (collider == null)
        {
            CircleCollider2D circleCollider = item.GetComponent<CircleCollider2D>();
            if (circleCollider == null)
            {
                this.addBoxCollider2D(item);
            }
        }

        return item;
    }

    public GameObject InstantiateObject(GameObject obj, Vector2 position, Transform parent)
    {
        GameObject item = Instantiate(obj);
        return FakeInstantiateObject(item, position, parent);
    }

    public GameObject InstantiateBorder(GameObject obj, Vector2 position, Transform parent)
    {
        GameObject borderObj = Instantiate(obj);
        borderObj.transform.position = position;
        borderObj.transform.parent = parent;
        return borderObj;
    }

    public BoxCollider2D addBoxCollider2D(GameObject obj)
    {
        BoxCollider2D collider = obj.AddComponent(typeof(BoxCollider2D)) as BoxCollider2D;
        collider.size = this.getGameObject2DBoxColliderSize();
        return collider;
    }

    public GameObject InstantiateItem(GameObject itemType, Vector2 position)
    {
        Vector2 realPosition = this.getRealPosition(position.x, position.y);
        int id1 = PhotonNetwork.AllocateViewID();

        PhotonView photonView = this.GetComponent<PhotonView>();
        photonView.RPC("SpawnOnNetwork", PhotonTargets.All, realPosition, id1);
        return null;
        //GameObject item = this.InstantiateObject(itemType, realPosition, this.itemsHolder.transform);
        //GameObject item = PhotonNetwork.Instantiate(itemType.gameObject.name, realPosition, Quaternion.identity, 0);
        //item.transform.parent = this.itemsHolder.transform;
        //return item;
    }

    [PunRPC]
    void SpawnOnNetwork(Vector2 pos, int id1)
    {
        GameObject newPlayer = GameObject.Instantiate(lifeItem, pos, Quaternion.identity) as GameObject;
        newPlayer.transform.parent = this.itemsHolder.transform;
        // Set player's PhotonView
        PhotonView[] nViews = newPlayer.transform.GetComponentsInChildren<PhotonView>();
        nViews[0].viewID = id1;
    }

    public SpawnPointGenerator getSpawnPointGenerator()
    {
        return this.spawnPointGenerator;
    }

    public void SetSeed(string seed)
    {
        this.seed = seed;
    }

    private void SetUpGameProperties()
    {
        /* Calculate size of a Square on a map */
        this.squareSize = this.squarePixelSize / 100.0f;
        this.gameObject2DGameObjectSize = new Vector2(squareSize, squareSize);
    }

    // ---------------------- Public Methods --------------------

    public float getSquareSize()
    {
        return this.squareSize;
    }

    public Vector2 getGameObject2DBoxColliderSize()
    {
        return this.gameObject2DGameObjectSize;
    }

    /*
    * Using the Fake postion of an object it returns the real one, because in the this.map[1,1] could not be in the coordinate 1,1 on the game. It depends of the PIXEL SIZE!!
    */
    public Vector2 getRealPosition(float x, float y)
    {
        return new Vector2(x * this.squareSize, y * this.squareSize);
    }

    public Vector2 getRealPosition(Vector2 position)
    {
        return new Vector2(position.x * this.squareSize, position.y * this.squareSize);
    }

    /*
    * Using the Real postion of an object it returns the fake position that is used to access the this.map property. Because the this.map[1,1] could not be in the coordinate 1,1 on the game. It always depends of the PIXEL SIZE!!
    */
    public Vector2 getMapFakePosition(Vector2 realPosition)
    {
        float squareSize = this.getSquareSize();
        return new Vector2((int)(realPosition.x / squareSize), (int)(realPosition.y / squareSize));
    }
}
