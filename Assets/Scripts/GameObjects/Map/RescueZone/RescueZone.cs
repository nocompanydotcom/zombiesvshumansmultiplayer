﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RescueZone : MonoBehaviour {

    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        this.spriteRenderer = this.GetComponent<SpriteRenderer>();
    }

    public void Enable()
    {
        this.spriteRenderer.enabled = true;
    }

    public void Disable()
    {
        this.spriteRenderer.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if("Player".Equals(collision.tag))
        {
            PhotonCharacter character = collision.gameObject.GetComponent<PhotonCharacter>();
            if (character == null)
                Debug.LogError("No script denominated by 'PhotonCharacter' attached with the following GameObject '" + collision.gameObject.name + "'");
            else
            {
                character.RescueZoneCollision();
            }
        }
    }
}
