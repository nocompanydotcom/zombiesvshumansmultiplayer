﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointGenerator {

    public readonly Vector2 INVALID_SPAWN_POINT = new Vector2(-1, -1);
    private List<Vector2> emptyPoints;

    public SpawnPointGenerator()
    {

    }

	public SpawnPointGenerator(List<Vector2> emptyPoints)
    {
        this.emptyPoints = emptyPoints;
    }

    //------------------ Public functions --------------------

    public Vector2 generateRescueZoneSpawnPoint()
    {
        return this.generateItemSpawnPoint();
    }

    public Vector2 generateZombieSpawnPoint()
    {
        return generateRandomSpawnPoint();
    }

    public Vector2 generateHumanSpawnPoint()
    {
        return generateRandomSpawnPoint();
    }

    public Vector2 generateItemSpawnPoint()
    {
        int index = -1;
        Vector2 position = generateRandomSpawnPointAndSaveIndex(ref index);
        emptyPoints.RemoveAt(index);
        return position;
    }

    public void addEmptyPoint(Vector2 point)
    {
        this.emptyPoints.Add(point);
    }

    //------------------ Private functions --------------------

    private Vector2 generateRandomSpawnPoint()
    {
        int i = 0;
        return generateRandomSpawnPointAndSaveIndex(ref i);
    }

    private Vector2 generateRandomSpawnPointAndSaveIndex(ref int index)
    {
        if (emptyPoints == null || emptyPoints.Count == 0)
            return INVALID_SPAWN_POINT;

        index = Random.Range(0, emptyPoints.Count);
        return emptyPoints[index];
    }

    //------------------ Setters and Getters --------------------

    public void setEmptyPoints(List<Vector2> emptyPoints)
    {
        this.emptyPoints = emptyPoints;
    }

}
