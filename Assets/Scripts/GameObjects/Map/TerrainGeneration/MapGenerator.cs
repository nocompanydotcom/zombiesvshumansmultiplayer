﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MapGenerator
{

    private static float FLOOR_MARGIN = 0.5f;
    private GameObject floorPrefab;
    private GameObject mainTerrainObject;
    private string floorSortingLayerName = "Floor";
    private int mapWidth = 400;
    private int mapHeight = 400;
    private uint randomAlgorithmFillPercentage = 50;
    private bool smoothMapAlgorithm;
    private bool removeNoiseAlgorithm;
    private uint minimumNoise = 2;
    private bool connectRoomsAlgorithm;
    private uint connectionDiameter = 2;

    //Borders
    private static uint BORDER_SPRITE_SIZE = 10;
    private Sprite borderSprite;
    private GameObject borderObject;
    private GameObject cornerBorderObject;
    private GameObject bordersParent;
    private bool isBordersCreated;

    //Floor (EX: Grass)
    private GameObject floorObject;
    private bool isFloorCreated;

    private float realMapWidth;
    private float realMapHeight;
    private GameObject[,] map; //Map with the real GameObjects of the Game Scene
    private GameObject[,] objectMap; //Used to incorporate all the objects of the map
    private List<Vector2> availablePositions;
    private GameObject obstaclesParentObject;
    private List<Vector2> emptyPoints;

    //Constants
    private readonly Vector2 NO_AVAILABLE_POSITIONS = new Vector2(-1, -1);
    private readonly int numberOfNeighbours = 4; //Just stay with the default

    public MapGenerator(GameObject floorPrefab, GameObject mainTerrainObject, string floorSortingLayerName,
                        int mapWidth, int mapHeight, uint randomAlgorithmFillPercentage, bool smoothMapAlgorithm, bool removeNoiseAlgorithm,
                        uint minimumNoise, bool connectRoomsAlgorithm, uint connectionDiameter, Sprite borderSprite, GameObject borderObject, GameObject cornerBorderObject)
    {
        this.floorPrefab = floorPrefab;
        this.mainTerrainObject = mainTerrainObject;
        this.floorSortingLayerName = floorSortingLayerName;
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
        this.randomAlgorithmFillPercentage = randomAlgorithmFillPercentage;
        this.smoothMapAlgorithm = smoothMapAlgorithm;
        this.removeNoiseAlgorithm = removeNoiseAlgorithm;
        this.minimumNoise = minimumNoise;
        this.connectRoomsAlgorithm = connectRoomsAlgorithm;
        this.connectionDiameter = connectionDiameter;
        this.borderSprite = borderSprite;
        this.borderObject = borderObject;
        this.cornerBorderObject = cornerBorderObject;

        this.isBordersCreated = false;
        this.isFloorCreated = false;
    }

    // Use this for initialization
    public void Start(string seed)
    {
        InitializeVariables();

        MapFloorGeneration(); //Generates the floor of the game

        RandomObjectMapGeneration(seed, this.mainTerrainObject, 5000); //Generates random places for the main sprite object to be dropped
        if (this.smoothMapAlgorithm)
            SmoothObjectMap(this.mainTerrainObject); //Smooth the map of the main sprite object
        if (this.removeNoiseAlgorithm)
            RemoveNoiseFromMap(this.mainTerrainObject, this.minimumNoise);
        if (this.connectRoomsAlgorithm)
            ConnectEmptyRegionsOfMap(null, this.connectionDiameter);

        this.emptyPoints = RenderObjectsOfMapAndRetrieveEmptyPoints();
    }

    private void InitializeVariables()
    {
        /* Create Obstacles GameObject - Will hold every obstacle such as trees and bushes from the map */
        this.obstaclesParentObject = new GameObject("Obstacles");
        this.obstaclesParentObject.transform.position = Vector3.zero;
        this.obstaclesParentObject.transform.parent = MapController.instance.transform;

        /* Calculate real size of the Map related with the Square size */
        float squareSize = MapController.instance.getSquareSize();
        this.realMapWidth = this.mapWidth * squareSize;
        this.realMapHeight = this.mapHeight * squareSize;

        this.map = new GameObject[this.mapWidth, this.mapHeight];
        this.objectMap = new GameObject[this.mapWidth, this.mapHeight];
        this.availablePositions = getFakeTotalPossibleAvailablePositions();
    }

    private void MapFloorGeneration()
    {
        GenerateBorders();

        if (this.isFloorCreated)
            return;

        string name = "Floor";
        Transform parent = MapController.instance.transform;
        Vector2 position = new Vector2(-FLOOR_MARGIN, -FLOOR_MARGIN);
        Vector2 size = new Vector2(this.realMapWidth + FLOOR_MARGIN + FLOOR_MARGIN, this.realMapHeight + FLOOR_MARGIN + FLOOR_MARGIN);
        this.floorObject = GenerateFloor(this.floorPrefab, name, position, parent, size);
        this.isFloorCreated = true;
    }

    private GameObject GenerateFloor(GameObject prefab, string name, Vector2 position, Transform parent, Vector2 size)
    {
        //Create Game Object for the Sprite Renderer
        GameObject obj = GameObject.Instantiate(prefab);
        obj.name = name;
        obj.transform.parent = parent;
        obj.transform.position = position;

        SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>();
        if(renderer != null)
        {
            renderer.size = size;
        }

        return obj;
    }

    private GameObject GenerateTiledObject(Sprite sprite, string name, Vector2 position, Transform parent, Vector2 size)
    {
        //Create Game Object for the Sprite Renderer
        GameObject obj = new GameObject(name);
        //floorObj.transform.position = Vector3.zero;
        obj.transform.position = position;
        obj.transform.parent = parent;

        //Attach a SpriteRenderer to the Object
        SpriteRenderer render = Render.getSpriteRenderer(obj, sprite, this.floorSortingLayerName);
        render.drawMode = SpriteDrawMode.Tiled;
        render.size = size;

        return obj;
    }

    private void GenerateBorders()
    {
        if (this.isBordersCreated)
            return;

        //Create parent holder of borders objects
        this.bordersParent = new GameObject("Borders");
        bordersParent.transform.parent = MapController.instance.transform;

        GameObject leftBorder = new GameObject();
        leftBorder.transform.localScale = new Vector2(1, this.mapHeight);
        Vector2 realPosition = MapController.instance.getRealPosition(-0.5f, 0);
        leftBorder.name = "left-border";
        this.addSideBordersBoxCollider2D(leftBorder);
        MapController.instance.FakeInstantiateObject(leftBorder, realPosition, bordersParent.transform);

        GameObject rightBorder = new GameObject();
        rightBorder.transform.localScale = new Vector2(1, this.mapHeight);
        realPosition = MapController.instance.getRealPosition(this.mapWidth + 0.5f, 0);
        rightBorder.name = "right-border";
        this.addSideBordersBoxCollider2D(rightBorder);
        MapController.instance.FakeInstantiateObject(rightBorder, realPosition, bordersParent.transform);

        GameObject topBorder = new GameObject();
        topBorder.transform.localScale = new Vector2(this.mapWidth + 2, 1);
        realPosition = MapController.instance.getRealPosition(-0.5f, this.mapHeight + 0.5f);
        topBorder.name = "top-border";
        this.addTopAndBottomBoxCollider2D(topBorder);
        MapController.instance.FakeInstantiateObject(topBorder, realPosition, bordersParent.transform);

        GameObject bottomBorder = new GameObject();
        bottomBorder.transform.localScale = new Vector2(this.mapWidth + 2, 1);
        realPosition = MapController.instance.getRealPosition(-0.5f, -0.5f);
        bottomBorder.name = "bottom-border";
        this.addTopAndBottomBoxCollider2D(bottomBorder);
        MapController.instance.FakeInstantiateObject(bottomBorder, realPosition, bordersParent.transform);

        GenerateBorderLayout(bordersParent);
        this.isBordersCreated = true;
    }

    private void GenerateBorderLayout(GameObject bordersObj)
    {
        GameObject bordersLayoutParentObj = new GameObject("Layout");
        bordersLayoutParentObj.transform.parent = bordersObj.transform;

        Vector2 position;
        GameObject borderObject;
        //Left border layout
        for (int y = 0; y < this.mapHeight; y++)
        {
            position = MapController.instance.getRealPosition(new Vector2(-1, y));
            borderObject = MapController.instance.InstantiateBorder(this.borderObject, position, bordersLayoutParentObj.transform);
        }

        //Right border layout
        for (int y = 0; y < this.mapHeight; y++)
        {
            position = MapController.instance.getRealPosition(new Vector2(this.mapWidth, y));
            borderObject = MapController.instance.InstantiateBorder(this.borderObject, position, bordersLayoutParentObj.transform);
        }

        //Top border layout
        for (int x = 1; x < this.mapWidth + 1; x++)
        {
            position = MapController.instance.getRealPosition(new Vector2(x, this.mapHeight));
            borderObject = MapController.instance.InstantiateBorder(this.borderObject, position, bordersLayoutParentObj.transform);
            borderObject.transform.Rotate(new Vector3(0, 0, 90));
        }

        //Bottom border layout
        for (int x = 1; x < this.mapWidth + 1; x++)
        {
            position = MapController.instance.getRealPosition(new Vector2(x, -1));
            borderObject = MapController.instance.InstantiateBorder(this.borderObject, position, bordersLayoutParentObj.transform);
            borderObject.transform.Rotate(new Vector3(0, 0, 90));
        }

        //Bottom Left corner
        position = MapController.instance.getRealPosition(new Vector2(-1, -1));
        borderObject = MapController.instance.InstantiateBorder(this.cornerBorderObject, position, bordersLayoutParentObj.transform);

        //Top Left corner
        position = MapController.instance.getRealPosition(new Vector2(-1, this.mapHeight + 1));
        borderObject = MapController.instance.InstantiateBorder(this.cornerBorderObject, position, bordersLayoutParentObj.transform);
        borderObject.transform.Rotate(new Vector3(0, 0, 270));

        //Bottom Right corner
        position = MapController.instance.getRealPosition(new Vector2(this.mapWidth + 1, -1));
        borderObject = MapController.instance.InstantiateBorder(this.cornerBorderObject, position, bordersLayoutParentObj.transform);
        borderObject.transform.Rotate(new Vector3(0, 0, 90));

        //Top Right corner
        position = MapController.instance.getRealPosition(new Vector2(this.mapWidth + 1, this.mapHeight + 1));
        borderObject = MapController.instance.InstantiateBorder(this.cornerBorderObject, position, bordersLayoutParentObj.transform);
        borderObject.transform.Rotate(new Vector3(0, 0, 180));

        //Generate water/sprite effect
        GameObject waterParent = new GameObject("water-parent");
        waterParent.transform.parent = bordersLayoutParentObj.transform;
        Transform parent = waterParent.transform;
        float padding = 0.5f;

        Vector2 size = new Vector2(BORDER_SPRITE_SIZE, BORDER_SPRITE_SIZE + BORDER_SPRITE_SIZE + this.realMapHeight);
        position = new Vector2(-BORDER_SPRITE_SIZE - padding, -BORDER_SPRITE_SIZE);
        GenerateTiledObject(this.borderSprite, "water-left", position, parent, size);

        size = new Vector2(BORDER_SPRITE_SIZE, BORDER_SPRITE_SIZE + BORDER_SPRITE_SIZE + this.realMapHeight);
        position = new Vector2(this.realMapWidth + padding, -BORDER_SPRITE_SIZE);
        GenerateTiledObject(this.borderSprite, "water-right", position, parent, size);

        size = new Vector2(BORDER_SPRITE_SIZE + BORDER_SPRITE_SIZE + this.realMapWidth, BORDER_SPRITE_SIZE);
        position = new Vector2(-BORDER_SPRITE_SIZE, this.realMapHeight + padding);
        GenerateTiledObject(this.borderSprite, "water-top", position, parent, size);

        size = new Vector2(BORDER_SPRITE_SIZE + BORDER_SPRITE_SIZE + this.realMapWidth, BORDER_SPRITE_SIZE);
        position = new Vector2(-BORDER_SPRITE_SIZE, -BORDER_SPRITE_SIZE - padding);
        GenerateTiledObject(this.borderSprite, "water-bottom", position, parent, size);
    }

    //This functionality was based on https://www.youtube.com/watch?v=v7yyZZjF1z4
    private void RandomObjectMapGeneration(string seed, GameObject sprite, int numOfObjects)
    {
        System.Random pseudoRandom = new System.Random(seed.GetHashCode());
        for (int x = 0; x < this.mapWidth; x++)
        {
            for (int y = 0; y < this.mapHeight; y++)
            {
                if (x == 0 || x == mapWidth - 1 || y == 0 || y == mapHeight - 1)
                    this.objectMap[x, y] = sprite;
                else
                {
                    this.objectMap[x, y] = (pseudoRandom.Next(0, 100) < this.randomAlgorithmFillPercentage) ? sprite : null;
                }
            }
        }
    }

    //This functionality was based on https://www.youtube.com/watch?v=v7yyZZjF1z4
    private void SmoothObjectMap(GameObject obj)
    {
        for (int x = 0; x < this.mapWidth; x++)
        {
            for (int y = 0; y < this.mapHeight; y++)
            {
                int numOfNeighbours = getObjectNeighboursCount(x, y);

                if (numOfNeighbours > this.numberOfNeighbours)
                    this.objectMap[x, y] = obj;
                else if (numOfNeighbours < this.numberOfNeighbours)
                    this.objectMap[x, y] = null;
            }
        }
    }

    //Remove minimum size (inclusive) of regions/noises on the map
    //This functionality were based on https://www.youtube.com/watch?v=xYOG8kH2tF8&list=PLFt_AvWsXl0eZgMK_DT5_biRkWXftAOf9&index=5
    private void RemoveNoiseFromMap(GameObject obj, uint minNoise)
    {
        for (int x = 0; x < this.mapWidth; x++)
        {
            for (int y = 0; y < this.mapHeight; y++)
            {
                List<Vector2> region = getRegionPointsAsList(obj, x, y);
                if (region.Count <= minNoise)
                {
                    removeRegionPoints(region);
                }
            }
        }
    }

    private void ConnectEmptyRegionsOfMap(GameObject obj, uint connectionThickness)
    {
        List<Map.Region> regions = this.getMapEmptyRegions();
        this.connectRegions(regions, obj, this.mapWidth, this.mapHeight, connectionThickness);
    }

    private List<Vector2> RenderObjectsOfMapAndRetrieveEmptyPoints()
    {
        List<Vector2> emptyPoints = new List<Vector2>();
        for (int x = 0; x < this.mapWidth; x++)
        {
            int orderLayer = this.mapWidth;
            for (int y = 0; y < this.mapHeight; y++)
            {
                GameObject obj = this.objectMap[x, y];
                if (obj == null)
                {
                    emptyPoints.Add(new Vector2(x, y));
                }
                else
                {
                    renderObject(obj.name + "_" + x + "_" + y, obj, MapController.instance.getRealPosition(x, y), orderLayer);
                }
                orderLayer -= 1;
            }
        }
        return emptyPoints;
    }
    /**
     ********************** AUXILIARY METHODS **********************
    */

    /**
    ********************** Connecting rooms **********************
    */
    /**
     * Retrieves a List<Map.Region> that doesnt contain any Object/Sprite on this.objectMap
     * 
     * Check function for more information:
     *      MapGenerator.getRegionPointsAsList(null, x, y)
     */
    private List<Map.Region> getMapEmptyRegions()
    {
        List<Map.Region> regions = new List<Map.Region>();

        for (int x = 0; x < this.mapWidth; x++)
        {
            for (int y = this.mapHeight - 1; y >= 0; y--)
            {
                List<Vector2> regionPoints = getRegionPointsAsList(null, x, y);
                if (regionPoints.Count > 0)
                {
                    if (!Map.Region.hasRegionPointsOnList(regions, regionPoints))
                    {
                        Map.Region r = new Map.Region(regionPoints);
                        regions.Add(r);
                    }
                }
            }
        }
        return regions;
    }
    /**
     * It connects Regions between them.
     * It's used for the map generated being accessible for all the Empty regions.
     */
    private void connectRegions(List<Map.Region> regions, GameObject prefabToConnection, int mapWidth, int mapHeight, uint connectionThickness)
    {
        regions = Map.Region.sortRegionsByDistanceFromFirst(regions);
        for (int i = 0; i < regions.Count - 1; i++)
        {
            Map.Region A = regions[i],
                   B = regions[i + 1];

            Vector2[] points = Map.Region.getClosestPointsFromRegions(A, B); //Debug.DrawLine(points[0], points[1], Color.yellow, 100); NOT CORRECT THE DEBUG.DrawLine POINTS

            //y = m*x + b
            List<Vector2> linePoints = StraightLineEquation.getPointsBetween(points[0], points[1], mapWidth, mapHeight, (int)connectionThickness);
            setPrefabToRegionPoints(linePoints, prefabToConnection);
        }
    }

    /**
     ********************** Rendering **********************
    */
    private void renderObject(string objectName, GameObject obj, Vector2 realPosition, int orderLayer)
    {
        GameObject createdObj = MapController.instance.InstantiateObject(obj, realPosition, this.obstaclesParentObject.transform);
        createdObj.name = objectName;
        SpriteRenderer renderer = createdObj.GetComponent<SpriteRenderer>();
        if (renderer != null)
        {
            renderer.sortingOrder = orderLayer;
        }

        Vector2 fakePosition = getMapFakePosition(realPosition);
        this.map[(int)fakePosition.x, (int)fakePosition.y] = obj;
    }

    /**
     ********************** Map Smooth functions **********************
    */
    /*
     * Get number of neighbours that are not null in this.objectMap
     */
    private int getObjectNeighboursCount(int xIndex, int yIndex)
    {
        int neighbours = 0,
            startXIndex = (xIndex == 0 ? 0 : xIndex - 1),
            startYIndex = (yIndex == 0 ? 0 : yIndex - 1),
            endXIndex = (xIndex == (this.mapWidth - 1) ? xIndex : xIndex + 1),
            endYIndex = (yIndex == (this.mapHeight - 1) ? yIndex : yIndex + 1);

        for (int x = startXIndex; x <= endXIndex; x++)
        {
            for (int y = startYIndex; y <= endYIndex; y++)
            {
                if (x == xIndex && y == yIndex)
                    continue;

                if (this.objectMap[x, y] != null) //Check if it exists any object
                {
                    neighbours++;
                }
            }
        }

        return neighbours;
    }

    /**
     ********************** Map Clean functions **********************
     */

    private void setPrefabToRegionPoints(List<Vector2> region, GameObject obj)
    {
        foreach (Vector2 v in region)
        {
            this.objectMap[(int)v.x, (int)v.y] = obj;
        }
    }

    private void removeRegionPoints(List<Vector2> region)
    {
        this.setPrefabToRegionPoints(region, null);
    }

    //Uses 'getRegionNeighboursRecursively' function
    private List<Vector2> getRegionPointsAsList(GameObject obj, int xIndex, int yIndex)
    {
        List<Vector2> region = new List<Vector2>();
        bool[,] analysedIndexes = new bool[this.mapWidth, this.mapHeight];

        //Add first element because must know that it's already being analysed.
        if (compareObjects(this.objectMap[xIndex, yIndex], obj))
        {
            analysedIndexes[xIndex, yIndex] = true;
            region.Add(new Vector2(xIndex, yIndex));

            region = getRegionPointsRecursively(ref region, ref analysedIndexes, obj, xIndex, yIndex);
        }

        return region;
    }

    //Note that the compararison method used to compare the SPRITEs are the method 'compareSprites(Sprite spr1, Sprite spr2)'
    private List<Vector2> getRegionPointsRecursively(ref List<Vector2> region, ref bool[,] analysedIndexes, GameObject obj, int xIndex, int yIndex)
    {
        int startXIndex = (xIndex == 0 ? 0 : xIndex - 1),
            startYIndex = (yIndex == 0 ? 0 : yIndex - 1),
            endXIndex = (xIndex == (this.mapWidth - 1) ? xIndex : xIndex + 1),
            endYIndex = (yIndex == (this.mapHeight - 1) ? yIndex : yIndex + 1);

        for (int x = startXIndex; x <= endXIndex; x++)
        {
            for (int y = startYIndex; y <= endYIndex; y++)
            {
                if (compareObjects(this.objectMap[x, y], obj) && !analysedIndexes[x, y]) //Check if it exists any object on this
                {
                    analysedIndexes[x, y] = true;

                    Vector2 position = new Vector2(x, y);
                    region.Add(position);

                    getRegionPointsRecursively(ref region, ref analysedIndexes, obj, x, y);
                }
            }
        }
        return region;
    }

    private void addSideBordersBoxCollider2D(GameObject obj)
    {
        BoxCollider2D collider = MapController.instance.addBoxCollider2D(obj);
        collider.offset = new Vector2(0, MapController.instance.getSquareSize() * 0.5f); //We use multiplication instead of division because multiplication should be more performant
    }

    private void addTopAndBottomBoxCollider2D(GameObject obj)
    {
        BoxCollider2D collider = MapController.instance.addBoxCollider2D(obj);
        collider.offset = new Vector2(MapController.instance.getSquareSize() * 0.5f, 0); //We use multiplication instead of division because multiplication should be more performant
    }

    /**
     ********************** Map utilities functions **********************
    */
    /*
     * Improve Function to dont give an isolated point where the user cant move
    */
    private Vector2 getFakeValidPosition()
    {
        int totalAvailablePositions = this.availablePositions.Count;
        if (totalAvailablePositions == 0)
            return NO_AVAILABLE_POSITIONS;

        int x = Random.Range(0, totalAvailablePositions);
        Vector2 vectorPosition = this.availablePositions[x];
        this.availablePositions.RemoveAt(x);

        return vectorPosition;
    }
    /*
    * Using the Real postion of an object it returns the fake position that is used to access the this.map property. Because the this.map[1,1] could not be in the coordinate 1,1 on the game. It always depends of the PIXEL SIZE!!
    */
    public Vector2 getMapFakePosition(Vector2 realPosition)
    {
        return MapController.instance.getMapFakePosition(realPosition);
    }
    /*
     * Generate and returns all the available positions from the map.
    */
    private List<Vector2> getFakeTotalPossibleAvailablePositions()
    {
        List<Vector2> totalPositions = new List<Vector2>(); //The initial size of the list would be: this.mapWidth * this.mapHeight
        for (int x = 0; x < this.mapWidth; x++)
        {
            for (int y = 0; y < this.mapHeight; y++)
            {
                totalPositions.Add(new Vector2(x, y)); //We do (x * this.mapHeight) to don't overrite in the next x index
            }
        }
        return totalPositions;
    }

    private bool compareObjects(GameObject objOne, GameObject objTwo)
    {
        return objOne == objTwo;
    }

    public List<Vector2> getEmptyPoints()
    {
        return this.emptyPoints;
    }

    public static string NextSeed()
    {
        return System.DateTime.Now.ToUniversalTime().ToString();
    }

    public GameObject GetObstaclesParent()
    {
        return this.obstaclesParentObject;
    }

    public GameObject GetBordersParent()
    {
        return this.bordersParent;
    }

    public GameObject GetFloorObject()
    {
        return this.floorObject;
    }
}
