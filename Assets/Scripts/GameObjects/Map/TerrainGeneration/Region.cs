﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Map
{
    public class Region
    {
        private List<Vector2> points;
        private Mesh mesh;

        private static readonly Vector2 INVALID_POINT = new Vector2(-1, -1);

        public Region(List<Vector2> points)
        {
            this.points = points;
            this.mesh = new Mesh();

            initMesh();
        }

        private void initMesh()
        {
            Vector3[] vertices = new Vector3[this.points.Count];
            for (int i = 0; i < this.points.Count; i++)
            {
                Vector2 v = this.points[i];
                vertices[i] = v;
            }
            this.mesh.vertices = vertices;
        }

        /**
         * Public functions
         */

        public Vector2 getCenter()
        {
            return this.mesh.bounds.center;
        }
        /**
         * It uses:
         * 
         * Region.getCenter()
         * Region.getClosestPoint(Vector2 point)
         */
        public Vector2 getClosestPoint(Region A)
        {
            Vector2 centerA = A.getCenter();
            return this.mesh.bounds.ClosestPoint(centerA);
        }
        /**
         * Retrieves the closest point of the Mesh.bounds.
         * 
         * WARNING: it's important to know that the mesh.bounds sometimes dont have the same values as the Mesh.vertices. So that's why we created the function 'getClosestPointFromVertices' to be used after.
         */
        public Vector2 getClosestPoint(Vector2 point)
        {
            return this.mesh.bounds.ClosestPoint(point);
        }

        public bool containsPoint(Vector2 point)
        {
            foreach (Vector2 v in this.points)
            {
                if (v == point)
                    return true;
            }
            return false;
        }
        /*
         * HOW IT WORKS: 
         * This function checks if it exists the first element of the 'regionPoints' in any of the Regions from 'regions' list.
         * Because different Regions can't never have same points that's why we only check for one point instead of all of them.
         */
        public static bool hasRegionPointsOnList(List<Region> regions, List<Vector2> regionPoints)
        {
            if (regionPoints.Count > 0)
            {
                Vector2 point = regionPoints[0];
                foreach (Region region in regions)
                {
                    if (region.containsPoint(point))
                        return true;
                }
            }

            return false;
        }
        /*Method that will retrieve a List of Regions ordered by distance. It works as follows:
         * 
         * 1- It select a first element;
         * 2- Then it get the next region, if it contains more regions on the list, from that element selected previously;
         * 3- THen it selects that second element selected and then search for another the most near to that one;
         * 4- THen it repeats step 2 followed by 3 until no regions exists on the list
         * 
         * 
         * It used the function Region.nextNearestRegion(...)
        */
        public static List<Region> sortRegionsByDistanceFromFirst(List<Region> regions)
        {
            List<Region> sortedRegions = new List<Region>();

            Region nearRegion = null;
            while ((nearRegion = Region.nextNearestRegion(nearRegion, regions, sortedRegions)) != null)
            {
                sortedRegions.Add(nearRegion);
            }

            return sortedRegions;
        }
        /**
         * This function is used to go throw all the Regions on 'regions' list and retrieve the one that is the most near to 'prevRegion'.
         * 
         * Also it's important to notify that the 'ignoreRegions' list are the Regions to be ignored when comparing the distances.
         */
        public static Region nextNearestRegion(Region prevRegion, List<Region> regions, List<Region> ignoreRegions)
        {
            Region mostNear = null;

            if (regions.Count > 0)
            {
                if (prevRegion == null)
                    mostNear = regions[0];
                else
                {
                    for (int i = 1; i < regions.Count; i++) //Start in startIndex + 1, because we already initialize mostNear to the first Region found
                    {
                        Region currentRegion = regions[i];
                        if (prevRegion != currentRegion && !ignoreRegions.Contains(currentRegion))
                        {
                            if (mostNear == null)
                                mostNear = currentRegion;

                            else mostNear = Region.getNearestRegion(prevRegion, mostNear, currentRegion); //Assign the most near region
                        }
                    }
                }
            }

            return mostNear;
        }
        /**
         * This function is used to retrieve the most near Region ('regionOne' or 'regionTwo') comparing the distances of them between 'mainRegion'
         */
        public static Region getNearestRegion(Region mainRegion, Region regionOne, Region regionTwo)
        {
            Vector2[] closestPointsRegionOne = Region.getClosestPointsFromRegions(mainRegion, regionOne),
                      closestPointsRegionTwo = Region.getClosestPointsFromRegions(mainRegion, regionTwo);

            Region mostNear = null;
            if (Vector2.Distance(closestPointsRegionOne[0], closestPointsRegionOne[1]) < Vector2.Distance(closestPointsRegionTwo[0], closestPointsRegionTwo[1]))
                mostNear = regionOne;
            else mostNear = regionTwo;

            return mostNear;
        }
        /*
         * Returns the Closest Points between two regions in an array of Length == 2
         */
        public static Vector2[] getClosestPointsFromRegions(Region regionOne, Region regionTwo)
        {
            Vector2 regionTwoCenter = regionTwo.getCenter(),
                    regionOneClosestPoint = regionOne.getClosestPoint(regionTwoCenter),
                    regionTwoClosestPoint = regionTwo.getClosestPoint(regionOneClosestPoint);

            regionOneClosestPoint = regionOne.getClosestPointFromVertices(regionOneClosestPoint);
            if (regionOneClosestPoint == Region.INVALID_POINT)
                Debug.LogError("An error occoured when trying to retrieve the most closest point.");

            regionTwoClosestPoint = regionTwo.getClosestPointFromVertices(regionTwoClosestPoint);
            if (regionTwoClosestPoint == Region.INVALID_POINT)
                Debug.LogError("An error occoured when trying to retrieve the most closest point.");

            Vector2[] closestPoints = new Vector2[2];
            closestPoints[0] = regionOneClosestPoint;
            closestPoints[1] = regionTwoClosestPoint;
            return closestPoints;
        }
        /**
         * This functions compares all the points of a region and returns the most near point, available in the Mesh.
         * It's necessary because the Mesh.bounds sometimes has points that doesn't exists on the Mesh. I dont know if it's a bug of Unity or not.
         */
        private Vector2 getClosestPointFromVertices(Vector2 point)
        {
            Vector2 closest = INVALID_POINT;
            float closest_distance = float.MinValue;
            foreach (Vector2 v in this.points)
            {
                float currrentDistance = Vector2.Distance(v, point);
                if (currrentDistance.Equals(0.0f)) //Not possible to be more near
                {
                    closest = v;
                    break;
                }
                else if (closest_distance < currrentDistance)
                {
                    closest = v;
                    closest_distance = currrentDistance;
                }
            }

            return closest;
        }
        /**
         * Specially used for Debug purposes.
         */
        public override string ToString()
        {
            string str = "Region points: \n\r";
            foreach (Vector2 v in this.points)
            {
                str += v.x + "," + v.y + "; ";
            }
            return str;
        }
    }

}
