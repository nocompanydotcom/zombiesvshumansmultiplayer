﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualShoot : MonoBehaviour {
    
    private Transform shotPrefab;
	private float shootingRate = 0.15f;
	private float shootCooldown;
	private PhotonCharacter character;
	private Animator animator;

    private void Update()
	{
		if (PhotonNetwork.room.IsGameOver())
			this.gameObject.SetActive(false);

		if (hasUserTouchedShootButton())
			OnUserTouch();
	}

	public void initField(PhotonCharacter character, Transform shotPrefab, float shootingRate, float shootCooldown)
	{
		this.character = character;
		this.animator = character.GetComponent<Animator>();
		this.shotPrefab = shotPrefab;
		this.shootingRate = shootingRate;
		this.shootCooldown = shootCooldown;
	}

	private bool hasUserTouchedShootButton()
	{
		if (Input.touchSupported)
		{
			Touch[] touches = Input.touches;
			foreach (Touch t in touches)
			{
				if (t.phase == TouchPhase.Began)
				{
					return true;
				}
			}
		}
		else
		{
			return Input.GetMouseButtonDown(0); //Check for mouse click
		}

		return false;
	}

	private void OnUserTouch()
	{
		if (character == null) {
			return;
		}

		if (shootCooldown > 0) 
		{
			shootCooldown -= Time.deltaTime;
		}

		if (CanShoot())
        {
            if (this.character.IsHuman() && this.character.GetBullets() <= 0)
                PlaySoundEmptyGun();
            else
                Shoot();
        }
	}

	public bool CanShoot()
	{
		return (!PhotonNetwork.room.IsGameModeHighscore() && this.shootCooldown <= 0 &&
			!this.character.IsDead() && !this.character.IsTransforming());
	}

    private void PlaySoundEmptyGun()
    {
        PhotonHuman human = (PhotonHuman)this.character;
        human.PlayEmptyGunSound();
    }

    public void Shoot()
	{
		if (!this.animator.GetBool("playerRunning"))
		{
			this.animator.SetTrigger("playerAttacking");
		}

		this.shootCooldown = shootingRate;
		this.character.PlayShotSound();
		this.character.DecreaseBullets();
		this.character.InstantiateBullet(this.character.IsFlipped(), this.character.GetInstanceID()); 
	}
}
