using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets._2D;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class PhotonCharacter : PhotonRespawnable
{
    protected uint bullets = 1;
    [SerializeField]
    private int pointsPerKill = 20;

    //Weapon Script
    [SerializeField]
    private Transform shotPrefab;
    [SerializeField]
    private float shootingRate = 0.15f;
    [SerializeField]
    private float shootCooldown = 0;

    //Health script
    [SerializeField]
    protected int initialHp;

    private Rigidbody2D rigidBody2D;
    protected bool isDead;
    protected bool isTransforming;

    [SerializeField]
    private Transform shootPoint;

    protected AudioSource bulletSound;
    private AudioSource deathSound;

    // UI Top Panels
    private Text countLives;
    private Text countBullets;
    private Text countScore;
	private Text countOnlinePlayers;

    private PhotonAnimatorView m_AnimatorView;  // local animatorView. set when we create our character in CreatePlayerObject()
    private Animator animator;
    private PlayerMovement m_PlayerMovement;
    protected PhotonView m_PhotonView;
    protected bool isControllable;
    protected bool isTrusted;

    private GameObject joystick;
    private GameObject virtualShoot;

    void Awake()
    {
        this.m_PlayerMovement = GetComponent<PlayerMovement>();
        this.m_PhotonView = GetComponent<PhotonView>();
        this.animator = GetComponent<Animator>();
        this.m_AnimatorView = GetComponent<PhotonAnimatorView>();
    }

    private void Start()
    {
        this.isControllable = m_PhotonView.isMine;
        this.isTrusted = this.isControllable;

        this.bullets = this.GetInitialBullets();
        if (!this.isControllable)
        {
            return;
        }

        PhotonNetwork.player.ResetHP(this.initialHp); //Reset HP
        PhotonNetwork.player.SetIsHuman(this.IsHuman());
        Transform shotSoundPrefab = this.transform.Find("BulletSoundNetwork");
        if (shotSoundPrefab == null)
        {
            Debug.LogError("No 'BulletSoundNetwork' GameObject child name on Player");
        }
        else
        {
            this.bulletSound = shotSoundPrefab.gameObject.GetComponent<AudioSource>();
        }

        AudioSource deathAudio = GetComponent<AudioSource>();
        if (deathAudio == null)
        {
            Debug.LogError("No Audio Source For the Character");
        }
        else
        {
            this.deathSound = deathAudio;
        }

        InitUI();
        SetUpAnimationsSyncType();
        InitControls();
        SetUpCameraFollowing();
        InitTopCounters();
    }

    private void Update()
    {
		if (PhotonNetwork.room.IsGameOver ()) {
			DisableUI ();
		} else {
			UpdateNumberPlayersOnline ();
		}
    }

    protected virtual void InitUI()
    {
        Debug.LogError("'InitUI' function not overrided!!");
    }

    private void SetUpAnimationsSyncType()
    {
        this.m_AnimatorView.SetParameterSynchronized("FIX_BUG", PhotonAnimatorView.ParameterType.Bool, PhotonAnimatorView.SynchronizeType.Discrete);
        this.m_AnimatorView.SetParameterSynchronized("playerRunning", PhotonAnimatorView.ParameterType.Trigger, PhotonAnimatorView.SynchronizeType.Discrete);
        this.m_AnimatorView.SetParameterSynchronized("playerAttacking", PhotonAnimatorView.ParameterType.Trigger, PhotonAnimatorView.SynchronizeType.Discrete);
        this.m_AnimatorView.SetParameterSynchronized("playerDie", PhotonAnimatorView.ParameterType.Trigger, PhotonAnimatorView.SynchronizeType.Discrete);
        this.m_AnimatorView.SetParameterSynchronized("playerIdle", PhotonAnimatorView.ParameterType.Trigger, PhotonAnimatorView.SynchronizeType.Discrete);
        this.m_AnimatorView.SetParameterSynchronized("playerTransform", PhotonAnimatorView.ParameterType.Trigger, PhotonAnimatorView.SynchronizeType.Discrete);
    }

    private void InitControls()
    {
        GameObject canvas = GameObject.Find("Canvas");
        if (canvas == null)
        {
            Debug.LogError("There is no GameObject Canvas");
        }
        else
        {
            Component[] components = canvas.GetComponentsInChildren(typeof(Transform), true);
            if (components.Length <= 0)
            {
                Debug.LogError("It doesn't exists BackgroundJoystick child on the Canvas");
            }
            else
            {
                int numScript = 2;
                foreach (Component c in components)
                {
                    if (c.name.Equals("BackgroundJoystick"))
                    {
                        GameObject joystickObject = c.gameObject;
                        this.joystick = joystickObject;
                        joystickObject.SetActive(true);
                        VirtualJoystick virtualJoystick = joystickObject.GetComponent<VirtualJoystick>();
                        if (virtualJoystick == null)
                            Debug.LogError("No Canvas script attached to Canvas/BackgroundJoystick");

                        PlayerMovement playerMovement = this.GetComponent<PlayerMovement>();
                        playerMovement.DisableDestroy();
                        playerMovement.SetVirtualJoystick(virtualJoystick);
                        numScript--;
                    }
                    else if (c.name.Equals("VirtualShootButton"))
                    {
                        GameObject shootObject = c.gameObject;
                        this.virtualShoot = shootObject;
                        shootObject.SetActive(true);
                        VirtualShoot virtualShoot = shootObject.GetComponent<VirtualShoot>();
                        if (virtualShoot == null)
                            Debug.LogError("No Canvas script attached to Canvas/VirtualShootButton");
                        else
                            virtualShoot.initField(this, shotPrefab, shootingRate, shootCooldown);

                        numScript--;
                    }
                    if (numScript == 0)
                        break;
                }
            }
        }
    }

    protected virtual void DisableUI()
    {
        Debug.LogError("'DisableUI' not overrided");
    }

    public void InstantiateBullet(bool isLeft, int instanceID)
    {
        Quaternion rotation = Quaternion.identity;

        if (isLeft)
        {
            rotation.Set(0, 180.0f, 0, 0);
        }

        GameObject bullet = PhotonNetwork.Instantiate(this.shotPrefab.gameObject.name, this.shootPoint.position, rotation, 0);
        PhotonShotBullet photonShotBullet = bullet.GetComponent<PhotonShotBullet>();
        photonShotBullet.SetOwnerPhotonViewID(this.m_PhotonView.viewID);
    }

    [PunRPC]
    public virtual void RpcTakeDamage(int shooterPhotonViewID, string tag, int damage)
    {
        int hp = this.m_PhotonView.owner.DecreaseHP(damage);
        if (hp <= 0 && !isDead)
        {
            this.Die();
            this.NotifyKill(shooterPhotonViewID);
        }
        UpdateTopPanelLives();
    }

    protected void NotifyKill(int shooterPhotonViewID)
    {
        if (this.isTrusted)
        {
            PhotonView shooterPhotonView = PhotonView.Find(shooterPhotonViewID);
            if (shooterPhotonView == null)
                Debug.LogError("Shooter 'PhotonView' not founded. PhotonView ID: " + shooterPhotonView);

            else
            {
                shooterPhotonView.RPC("AddKillScore", PhotonTargets.All, this.IsHuman(), this.pointsPerKill);
            }
        }
    }

    private void Die()
    {
        this.isDead = true;
        this.animator.SetTrigger("playerDie");
        this.PlayDeathSound();
        if (this.isControllable)
            this.DelaySpawn();
    }

    public override void Spawn()
    {
        if (this.isTrusted)
        {
            this.m_PhotonView.RPC("RpcResetCharacter", PhotonTargets.All, null);
            Respawn();
        }
    }

    [PunRPC]
    public void RpcResetCharacter()
    {
        this.isDead = false;
        this.SetIsTransforming(false);
        this.m_PhotonView.owner.ResetHP(this.initialHp);
        this.ResetAllAnimationTriggers();
        this.animator.SetTrigger("playerIdle");
    }

    public void ResetAllAnimationTriggers()
    {
        this.animator.ResetTrigger("playerDie");
        this.animator.ResetTrigger("playerAttacking");
        this.animator.ResetTrigger("playerTransform");
        this.animator.ResetTrigger("playerRunning");
        this.animator.ResetTrigger("playerIdle");
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }

    public virtual void Respawn() { Debug.LogError("'CmdRespawn' function not overrided."); }

    public void AddCharacterLife()
    {
        this.m_PhotonView.owner.AddHP(1);
        this.UpdateTopPanelLives();
    }

    private void InitTopCounters()
    {
        GameObject canvas = GameObject.Find("Canvas");
        if (canvas == null)
        {
            Debug.LogError("There is no GameObject Canvas");
        }
        else
        {
            Component[] components = canvas.GetComponentsInChildren(typeof(Transform), false);

            if (components.Length > 0)
            {
                foreach (Component c in components)
                {
					if (c.name.Equals ("OnlinePlayers"))
					{
						countOnlinePlayers = c.gameObject.GetComponent<Text> ();
						countOnlinePlayers.text = "Players online: " + PhotonNetwork.room.PlayerCount;
					}

                    if (c.name.Equals("ScoreText"))
                    {
                        countScore = c.gameObject.GetComponent<Text>();
                        countScore.text = "" + this.m_PhotonView.owner.GetScore();
                    }

                    if (c.name.Equals("LivesText"))
                    {
                        countLives = c.gameObject.GetComponent<Text>();
                        countLives.text = "" + this.m_PhotonView.owner.GetTotalHP(); //  "Lives: " + "\u221E";
                    }

                    if (c.name.Equals("BulletsText"))
                    {
                        countBullets = c.gameObject.GetComponent<Text>();
                        if (IsHuman())
                        {
                            countBullets.text = "" + this.bullets;
                        }
                        else
                        {
                            countBullets.text = "\u221E";
                        }
                    }
                }
            }
        }
    }

	// Update label players online
	protected void UpdateNumberPlayersOnline()
	{
		if (!this.isControllable)
			return;

		if (this.countOnlinePlayers == null)
			return;

		countOnlinePlayers.text = "Players online: " + PhotonNetwork.room.PlayerCount;
	}

    /*
     * WARNING: This function only updates the bullets panel if the user 'this.isControllable' is true (if it's own gameplayer.
     */
    protected void UpdateTopPanelBullets()
    {
        if (!this.isControllable)
            return;

        if (this.countBullets == null)
            return;

        if (IsHuman())
        {
            countBullets.text = "" + this.bullets;
        }
        else
        {
            countBullets.text = "\u221E";
        }
    }
    /*
     * Auxiliary function to Set-up all obcjects state by tag name
    */
    protected void SetTopPanelGameObjectsActiveStateByTag(string tag, bool state)
    {
        GameObject controls = GameObject.FindGameObjectWithTag("Controls");
        if (controls == null)
            Debug.LogError("No GameObject's exists with 'Controls' tag");
        else
        {
            Component[] components = controls.GetComponentsInChildren(typeof(Transform), true);
            foreach (Component c in components)
            {
                if (c.tag.Equals(tag))
                {
                    c.gameObject.SetActive(state);
                }
            }
        }
    }
    /*
     * WARNING: This function only updates the lives panel if the user 'this.isControllable' is true (if it's own gameplayer.
     */
    public void UpdateTopPanelLives()
    {
        if (!this.isControllable)
            return;

        countLives.text = "" + this.m_PhotonView.owner.GetTotalHP();
    }
    /*
     * WARNING: This function only updates the kills panel if the user 'this.isControllable' is true (if it's own gameplayer.
     */
    public void UpdateTopPanelScore()
    {
        if (!this.isControllable)
            return;

        countScore.text = "" + this.m_PhotonView.owner.GetScore();
    }

    public void TransformCharacter()
    {
        this.SetIsTransforming(true);
        this.animator.SetTrigger("playerTransform");

        if (this.isControllable)
            PhotonPlayersController.instance.TransformPlayerWithDelay(this);
    }

    private void SetUpCameraFollowing()
    {
        Camera2DFollow follow = Camera.main.gameObject.GetComponent<Camera2DFollow>();
        if (follow == null)
        {
            Debug.LogError("No Camera2DFollow script assigned to the MainCamera");
        }
        else
        {
            follow.SetTarget(this.transform);
            follow.StartFollowing();
        }
    }

    public void PlayShotSound()
    {
        this.bulletSound.Play();
    }

    public void PlayDeathSound()
    {
        if (this.isControllable)
            this.deathSound.Play();
    }

    [PunRPC]
    public void AddKillScore(bool isHuman, int pointsPerKill)
    {
        if (this.isControllable)
        {
            this.m_PhotonView.owner.AddKill();
            int currentScore = this.m_PhotonView.owner.GetScore();
            if (this.IsHuman() == isHuman)
            {
                if (currentScore < pointsPerKill)
                    PhotonNetwork.player.SetScore(0);
                else
                {
                    pointsPerKill = -1 * pointsPerKill; //Decrease points per kill
                    PhotonNetwork.player.AddScore(pointsPerKill);
                }
            }
            else
            {
                PhotonNetwork.player.AddScore(pointsPerKill);
            }

            UpdateTopPanelScore();
        }
    }

    public virtual void FreezeMovement()
    {
        this.GetComponent<PlayerMovement>().FreezeMovement();
    }

    public virtual void RescueZoneCollision()
    {
        Debug.LogError("Function 'RescueZoneCollision' not overrided!");
    }

    /******************** Getters and Setters ****************************/

    public virtual bool IsHuman()
    {
        return false;
    }

    public virtual void DecreaseBullets()
    {
        this.bullets -= 1;
        if (this.bullets < 0)
            this.bullets = 0;

        UpdateTopPanelBullets();
    }

    public void DecreaseLife()
    {
        int hp = this.m_PhotonView.owner.DecreaseHP(1);
        if (hp == 0 && !this.isDead)
        {
            this.Die();
        }

        UpdateTopPanelLives();
    }

    public uint GetBullets()
    {
        return this.bullets;
    }

    public virtual uint GetInitialBullets()
    {
        return this.bullets;
    }

    public void AddBullets(uint bullets)
    {
        this.bullets += bullets;
        this.UpdateTopPanelBullets();
    }

    public GameObject GetGameObject()
    {
        return this.gameObject;
    }

    public Transform GetTransform()
    {
        return this.transform;
    }

    public int GetHP()
    {
        return this.m_PhotonView.owner.GetTotalHP();
    }

    public void SetHP(int hp)
    {
        this.m_PhotonView.owner.ResetHP(hp);
    }

    public bool IsDead()
    {
        return this.isDead;
    }

    public void SetIsTransforming(bool status)
    {
        this.isTransforming = status;
    }

    public bool IsTransforming()
    {
        return this.isTransforming;
    }

    public bool IsFlipped()
    {
        return this.m_PlayerMovement.IsFlipped();
    }

    public PhotonView GetPhotonView()
    {
        return this.m_PhotonView;
    }

    public int GetOwnerId()
    {
        return this.m_PhotonView.ownerId;
    }
}
