﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PhotonHuman : PhotonCharacter
{
    [SerializeField]
    private uint numberOfBullets = 150;
    [SerializeField]
    private AudioClip emptyGunSound;
    
    [PunRPC]
    public override void RpcTakeDamage(int shooterPhotonViewID, string tag, int damage)
    {
        if (this.isTransforming) //Ignore any bullets when transforming
            return;

        if (tag.Equals("Zombie_Bullet"))
        {
            this.TransformCharacter();
            this.NotifyKill(shooterPhotonViewID);
        }
        else
        {
            base.RpcTakeDamage(shooterPhotonViewID, tag, damage);
        }
    }

    public void PlayEmptyGunSound()
    {
        this.bulletSound.PlayOneShot(emptyGunSound);
    }

    public override void DelaySpawn()
    {
        if (!this.isTrusted)
            return;

        this.delayRespawn.RespawnHuman();
    }

    public override bool IsHuman()
    {
        return true;
    }
    
    public override void Respawn()
    {
        if (this.isTrusted)
        {
            SpawnPointGenerator spawnPointGenerator = MapController.instance.getSpawnPointGenerator();
            Vector2 newPosition = spawnPointGenerator.generateHumanSpawnPoint(),
                realPosition = MapController.instance.getRealPosition(newPosition);


            this.transform.position = realPosition;
            this.UpdateTopPanelLives();
        }
    }

    public override uint GetInitialBullets()
    {
        return this.numberOfBullets;
    }

    protected override void InitUI()
    {
        this.SetTopPanelGameObjectsActiveStateByTag("UI_Human", true);
        this.SetTopPanelGameObjectsActiveStateByTag("UI_Zombie", false);
    }

    protected override void DisableUI()
    {
        this.SetTopPanelGameObjectsActiveStateByTag("UI_Human", false);
    }

    public override void RescueZoneCollision()
    {
        PhotonPlayersController.instance.PlayerRescued(this.m_PhotonView.owner);
    }
}
