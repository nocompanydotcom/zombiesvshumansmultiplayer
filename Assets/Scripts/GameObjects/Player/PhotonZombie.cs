﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PhotonZombie : PhotonCharacter
{

    public override bool IsHuman()
    {
        return false;
    }

    public override void DelaySpawn()
    {
        if (!this.isTrusted)
            return;

        this.delayRespawn.RespawnZombie();
    }
    
    public override void Respawn()
    {
		if (this.isTrusted && !PhotonNetwork.room.IsGameModeHighscore())
        {
            SpawnPointGenerator spawnPointGenerator = MapController.instance.getSpawnPointGenerator();
            Vector2 newPosition = spawnPointGenerator.generateZombieSpawnPoint(),
                realPosition = MapController.instance.getRealPosition(newPosition);


            this.transform.position = realPosition;
            this.UpdateTopPanelLives();
        }
    }

    public override void DecreaseBullets() //If it's a Zombie don't decrement nr of bullets
    {
        return;
    }

    protected override void InitUI()
    {
        this.SetTopPanelGameObjectsActiveStateByTag("UI_Zombie", true);
        this.SetTopPanelGameObjectsActiveStateByTag("UI_Human", false);
    }

    protected override void DisableUI()
    {
        this.SetTopPanelGameObjectsActiveStateByTag("UI_Zombie", false);
    }

    public override void RescueZoneCollision()
    {
        this.DecreaseLife();
    }
}