using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private Vector2 speed = new Vector2(3, 3);

    private PhotonCharacter character;
    private Animator m_Animator;
    private Rigidbody2D m_RigidBody;
    private VirtualJoystick virtualJoystick;
    private PhotonView m_PhotonView;

    private bool canMove;
    private bool isControllable;
    private DIRECTION currentDirection;
    private bool canBeDestroyed;

    private void Awake()
    {
        this.character = this.GetComponent<PhotonCharacter>();
        this.m_Animator = this.GetComponent<Animator>();
        this.m_RigidBody = this.GetComponent<Rigidbody2D>();
        this.m_PhotonView = this.GetComponent<PhotonView>();

        this.canBeDestroyed = true;
    }

    private void Start()
    {
        this.isControllable = this.m_PhotonView.isMine;
        this.currentDirection = DIRECTION.RIGHT;
        this.canMove = true;
    }

    public void FixedUpdate()
    {
        if (this.virtualJoystick == null)
            return;

        if (!this.isControllable)
            return;

        if (this.character.IsDead() || this.character.IsTransforming()
            || PhotonNetwork.room.IsGameOver() || PhotonNetwork.room.IsGameModeHighscore() || !canMove)
        {
            this.StopMovement();
            this.HideVirtualJoystick();

            return;
        }
        else this.ShowVirtualJoystick();

        //if (!canMove) {
        //			return;
        //	}

        float inputX = this.virtualJoystick.Horizontal();
        float inputY = this.virtualJoystick.Vertical();

        // Movimentacao pela direcao.
        Vector2 movement = new Vector2(
            inputX * this.speed.x,
            inputY * this.speed.y);

        if (movement != Vector2.zero)
        {
            this.m_Animator.SetTrigger("playerRunning");

            DIRECTION direction = DIRECTION.RIGHT;
            if (inputX < 0)
            {
                direction = DIRECTION.LEFT;
            }

            if (this.currentDirection != direction)
                this.Flip(direction);
        }

        this.m_RigidBody.velocity = movement;
    }

    private void OnDestroy()
    {
        if (!this.isControllable)
            return;

        if (!this.canBeDestroyed)
        {
            this.canBeDestroyed = true;
            return;
        }

        if (this.virtualJoystick != null)
            this.HideVirtualJoystick();
    }

    public void SetVirtualJoystick(VirtualJoystick virtualJoystick)
    {
        this.virtualJoystick = virtualJoystick;
    }

    public void StopMovement()
    {
        this.m_RigidBody.velocity = Vector2.zero;
        if (this.virtualJoystick != null)
        {
            this.virtualJoystick.ResetAllInputAxes();
        }
        else
        {
            Debug.Log("Virtual Joystick is NULL !!!!!!!!!!!!!!!!!!!!!!");
        }
    }

    public void FreezeMovement()
    {
        this.StopMovement();
        this.canMove = false;
        Invoke("UnFreezeMovement", 3.0f);
    }

    public void UnFreezeMovement()
    {
        this.canMove = true;
    }

    private void Flip(DIRECTION direction)
    {
        this.transform.Rotate(new Vector3(0.0f, 180.0f, 0.0f));
        this.currentDirection = direction;
    }

    public bool IsFlipped()
    {
        return (this.currentDirection == DIRECTION.LEFT);
    }

    public void DisableDestroy()
    {
        this.canBeDestroyed = false;
    }

    public void HideVirtualJoystick()
    {
        if (this.virtualJoystick != null)
            this.virtualJoystick.gameObject.SetActive(false);
    }

    public void ShowVirtualJoystick()
    {
        if (this.virtualJoystick != null)
            this.virtualJoystick.gameObject.SetActive(true);
    }

    public void ToggleVirtualJoystickState()
    {
        this.virtualJoystick.gameObject.SetActive(!this.virtualJoystick.gameObject.activeSelf);
    }
}