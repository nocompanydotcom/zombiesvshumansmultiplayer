﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RescueZoneLocator : MonoBehaviour {
    
    private Transform rescueZoneTransfom;
    private SpriteRenderer spriteRenderer;

    private void Update()
    {
        if (this.rescueZoneTransfom == null || !PhotonNetwork.player.IsHuman())
            return;
        
        PointArrowToVector(this.rescueZoneTransfom.transform.position);
    }

    public void Enable()
    {
        GameObject rescueZone = GameObject.FindGameObjectWithTag("RescueZone");
        if (rescueZone == null)
            Debug.LogError("No GameObject with tag 'RescueZone' specified");
        else
        {
            this.rescueZoneTransfom = rescueZone.transform;
            this.spriteRenderer = GetComponent<SpriteRenderer>();
        }
    }

    public void Disable()
    {
        this.rescueZoneTransfom = null;
        this.spriteRenderer = null;
    }

    /*
     * Code taken from: http://answers.unity3d.com/questions/384074/how-to-make-a-2d-gui-arrow-point-at-a-3d-object.html#answer-384740
    */
    private void PointArrowToVector(Vector3 targetPosition)
    {
        this.spriteRenderer.enabled = false;

        Vector3 v3Pos = Camera.main.WorldToViewportPoint(targetPosition);

        if (v3Pos.z < Camera.main.nearClipPlane)
            return;  // Object is behind the camera

        if (v3Pos.x >= 0.0f && v3Pos.x <= 1.0f && v3Pos.y >= 0.0f && v3Pos.y <= 1.0f)
            return; // Object center is visible

        this.spriteRenderer.enabled = true;
        v3Pos.x -= 0.5f;  // Translate to use center of viewport
        v3Pos.y -= 0.5f;
        v3Pos.z = 0;      // I think I can do this rather than do a 
                          //   a full projection onto the plane

        float fAngle = Mathf.Atan2(v3Pos.x, v3Pos.y);
        transform.localEulerAngles = new Vector3(0.0f, 0.0f, -fAngle * Mathf.Rad2Deg);

        v3Pos.x = 0.5f * Mathf.Sin(fAngle) + 0.5f;  // Place on ellipse touching 
        v3Pos.y = 0.5f * Mathf.Cos(fAngle) + 0.5f;  //   side of viewport
        v3Pos.z = Camera.main.nearClipPlane + 0.01f;  // Looking from neg to pos Z;
        transform.position = Camera.main.ViewportToWorldPoint(v3Pos);
    }
}
