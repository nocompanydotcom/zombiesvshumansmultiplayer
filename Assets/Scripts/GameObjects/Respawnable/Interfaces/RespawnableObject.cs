﻿
public interface RespawnableObject {
    
    void InvokeMethod(string method_name, int delay_time);
    void UnSpawn();
    void Spawn();
}
