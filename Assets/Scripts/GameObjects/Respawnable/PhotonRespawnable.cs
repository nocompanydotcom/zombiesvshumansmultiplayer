﻿using UnityEngine;
using UnityEngine.Networking;

public class PhotonRespawnable : MonoBehaviour
{

    protected DelayRespawn delayRespawn;

    public PhotonRespawnable()
    {
        this.delayRespawn = new DelayRespawn(this);
    }

    [PunRPC]
    public void RpcNotifyGameobjectState(GameObject obj, bool state)
    {
        obj.SetActive(state);
    }

    /*************************************************************************************************************/
    /************************************ RespawnableObject Interface methods ************************************/
    /*************************************************************************************************************/

    public void UnSpawn()
    {
        RpcNotifyGameobjectState(this.gameObject, false);
    }

    public virtual void Spawn()
    {
        RpcNotifyGameobjectState(this.gameObject, true);
    }

    public virtual void DelaySpawn()
    {
        Debug.LogError("No overriding of the function 'DelaySpawn'");
    }

    public void InvokeMethod(string methodName, int delay_time)
    {
        Invoke(methodName, delay_time);
    }
}
