﻿
public class DelayRespawn {
    
    //Seconds
    public static int RESPAWN_ITEM_DELAY_TIME = 3;
    public static int RESPAWN_HUMAN_DELAY_TIME = 5;
    public static int RESPAWN_ZOMBIE_DELAY_TIME = 5;

    private PhotonRespawnable obj;

    public DelayRespawn(PhotonRespawnable obj)
    {
        this.obj = obj;
    }

    public void RespawnZombie()
    {
        this.DelayedRespawn(DelayRespawn.RESPAWN_ZOMBIE_DELAY_TIME);
    }

    public void RespawnHuman()
    {
        this.DelayedRespawn(DelayRespawn.RESPAWN_HUMAN_DELAY_TIME);
    }

    public void RespawnItem()
    {
        this.DelayedRespawn(DelayRespawn.RESPAWN_ITEM_DELAY_TIME);
    }

    public void DelayedRespawn(int delay_time)
    {
        this.obj.InvokeMethod("Spawn", delay_time);
    }    
}
