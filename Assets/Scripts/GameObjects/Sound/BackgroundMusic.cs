﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackgroundMusic : MonoBehaviour {

    [SerializeField]
    private string MULTIPLAYER_SCENE_NAME = "MultiplayerScene";
    [SerializeField]
    private AudioClip clipForMultiplayerScene;

    private static bool AudioBegin = false;
    private AudioSource audioSource;
    private AudioClip defaultAudio;
    private bool hasChangedClip = false;

    private void Awake()
    {
        if(!AudioBegin)
        {
            PlaySound();
            DontDestroyOnLoad(this.gameObject);
            AudioBegin = true;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(!hasChangedClip && SceneManager.GetActiveScene().name == MULTIPLAYER_SCENE_NAME)
            ReplaceAudio(clipForMultiplayerScene);
        else if(hasChangedClip && SceneManager.GetActiveScene().name != MULTIPLAYER_SCENE_NAME)
            ReplaceAudio(defaultAudio);
	}

    private void ReplaceAudio(AudioClip clip)
    {
        hasChangedClip = !hasChangedClip;
        this.audioSource.Stop();
        this.audioSource.clip = clip;
        this.audioSource.Play();
    }

    private void PlaySound()
    {
        if(this.audioSource == null)
        {
            this.audioSource = this.gameObject.GetComponent<AudioSource>();
            if (this.audioSource == null)
                Debug.LogError("BackgroudMusic GameObject doesn't contains any 'AudioSource' component");
            else
            {
                this.defaultAudio = this.audioSource.clip;
                this.audioSource.Play();
            }
        }
    }
}
