﻿using UnityEngine;
using System.Collections;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public static class CharacterExtensions
{
	#region Type
	public static void SetIsHuman(this PhotonPlayer player, bool isHuman)
	{
		Hashtable property = new Hashtable();  // using PUN's implementation of Hashtable
		property[PhotonPlayerProperties.PLAYER_TYPE] = isHuman;

		player.SetCustomProperties(property);
	}

	public static bool IsHuman(this PhotonPlayer player)
	{
		object isHuman;
		if (player.CustomProperties.TryGetValue(PhotonPlayerProperties.PLAYER_TYPE, out isHuman))
		{
			return (bool)isHuman;
		}

		return false;
	}

	#endregion

	#region Rescued
	public static void SetHasRescued(this PhotonPlayer player, bool hasBeenRescued)
	{
		Hashtable property = new Hashtable();  // using PUN's implementation of Hashtable
		property[PhotonPlayerProperties.PLAYER_RESCUED] = hasBeenRescued;
		player.SetCustomProperties(property);
	}

	public static bool IsPlayerRescued(this PhotonPlayer player)
	{
		object hasBeenRescued;
		if (player.CustomProperties.TryGetValue(PhotonPlayerProperties.PLAYER_RESCUED, out hasBeenRescued))
		{
			return (bool)hasBeenRescued;
		}

		return false;
	}
	#endregion

	#region Kills
	public static int AddKill(this PhotonPlayer player)
	{
		int kills = player.GetTotalKills() + 1;
		Hashtable property = new Hashtable();  // using PUN's implementation of Hashtable
		property[PhotonPlayerProperties.PLAYER_KILLS] = kills;
		player.SetCustomProperties(property);

		return kills;
	}

	public static int ResetKills(this PhotonPlayer player)
	{
		int kills = 0;
		Hashtable property = new Hashtable();  // using PUN's implementation of Hashtable
		property[PhotonPlayerProperties.PLAYER_KILLS] = kills;
		player.SetCustomProperties(property);

		return kills;
	}

	public static int GetTotalKills(this PhotonPlayer player)
	{
		object kills;
		if (player.CustomProperties.TryGetValue(PhotonPlayerProperties.PLAYER_RESCUED, out kills))
		{
			return (int)kills;
		}

		return 0;
	}
	#endregion

	#region HP
	public static int AddHP(this PhotonPlayer player, int bonus)
	{
		int hp = player.GetTotalHP() + bonus;
		Hashtable property = new Hashtable();  // using PUN's implementation of Hashtable
		property[PhotonPlayerProperties.PLAYER_HP] = hp;
		player.SetCustomProperties(property);

		return hp;
	}

	public static int DecreaseHP(this PhotonPlayer player, int damage)
	{
		int hp = player.GetTotalHP() - damage;
		if (hp < 0)
			hp = 0;

		Hashtable property = new Hashtable();  // using PUN's implementation of Hashtable
		property[PhotonPlayerProperties.PLAYER_HP] = hp;
		player.SetCustomProperties(property);

		return hp;
	}

	public static int ResetHP(this PhotonPlayer player, int hp)
	{
		Hashtable property = new Hashtable();  // using PUN's implementation of Hashtable
		property[PhotonPlayerProperties.PLAYER_HP] = hp;
		player.SetCustomProperties(property);

		return hp;
	}

	public static int GetTotalHP(this PhotonPlayer player)
	{
		object kills;
		if (player.CustomProperties.TryGetValue(PhotonPlayerProperties.PLAYER_HP, out kills))
		{
			return (int)kills;
		}

		return 0;
	}
	#endregion

}