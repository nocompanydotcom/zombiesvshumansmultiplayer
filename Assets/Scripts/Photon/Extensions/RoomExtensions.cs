﻿using UnityEngine;
using System.Collections;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public static class RoomExtensions
{
    #region PlayersRescued
    public static int RemovePlayerHasRescued(this Room room, PhotonPlayer player)
    {
        player.SetHasRescued(false);

        Hashtable property = new Hashtable();  // using PUN's implementation of Hashtable
        int numRescuedPlayers = (int) room.CustomProperties[PhotonRoomProperties.NUM_RESCUED_PLAYERS] - 1;
        if (numRescuedPlayers < 0)
            numRescuedPlayers = 0;
        property[PhotonRoomProperties.NUM_RESCUED_PLAYERS] = numRescuedPlayers;
        room.SetCustomProperties(property);

        return numRescuedPlayers;
    }

    public static int AddPlayerHasRescued(this Room room, PhotonPlayer player)
    {
        player.SetHasRescued(true);

        Hashtable property = new Hashtable();  // using PUN's implementation of Hashtable
        int numRescuedPlayers = (int)room.CustomProperties[PhotonRoomProperties.NUM_RESCUED_PLAYERS] + 1;
        property[PhotonRoomProperties.NUM_RESCUED_PLAYERS] = numRescuedPlayers;
        room.SetCustomProperties(property);

        return numRescuedPlayers;
    }

    public static int GetNumOfRescuedPlayers(this Room room)
    {
        return (int)room.CustomProperties[PhotonRoomProperties.NUM_RESCUED_PLAYERS];        
    }

    #endregion

    #region GameMode

    public static GAME_MODE GetNextGameMode(this Room room)
    {
        GAME_MODE currentGameMode = room.GetCurrentGameMode();
        if (currentGameMode == GAME_MODE.NORMAL)
        {
            if (room.HasHumansAndZombies())
            {
                Debug.Log("HAS HUMANS !!!");
                return GAME_MODE.RESCUE_ZONE;
            } else
            {
                Debug.Log("NO HUMANS !!!");
                return GAME_MODE.HIGHSCORES;
            }
        }
        else if (currentGameMode == GAME_MODE.RESCUE_ZONE)
        {
            return GAME_MODE.HIGHSCORES;
        }
        else
        {
            return GAME_MODE.GAME_OVER;
        }
    }

    public static void SetGameMode(this Room room, GAME_MODE nextGameMode)
    {
        Hashtable properties = new Hashtable();
        properties[PhotonRoomProperties.GAME_MODE] = nextGameMode;

        room.SetCustomProperties(properties);
    }

    public static GAME_MODE GetCurrentGameMode(this Room room)
    {
        return (GAME_MODE)room.CustomProperties[PhotonRoomProperties.GAME_MODE];
    }

    public static GAME_MODE TryGetCurrentGameMode(this Room room)
    {
        if(room.CustomProperties.ContainsKey(PhotonRoomProperties.GAME_MODE))
        {
            return room.GetCurrentGameMode();
        }
        return GAME_MODE.NONE;
    }

    public static bool IsGameOver(this Room room)
    {
        return room.GetCurrentGameMode() == GAME_MODE.GAME_OVER;
    }

    public static bool IsGameModeHighscore(this Room room)
    {
        return room.GetCurrentGameMode() == GAME_MODE.HIGHSCORES;
    }

    #endregion

    #region Players

    public static bool HasHumansAndZombies(this Room room)
    {
        bool hasHumans = false;
        bool hasZombies = false;

        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            if (player.IsHuman())
            {
                hasHumans = true;
            }
            else
            {
                hasZombies = true;
            }

            if (hasHumans && hasZombies)
            {
                return true;
            }
        }

        return false;
    }

    #endregion

    #region
    public static string GetMapSeed(this Room room)
    {
        return room.CustomProperties[PhotonRoomProperties.MAP_SEED].ToString();
    }

    public static void SetMapSeed(this Room room, string seed)
    {
        room.CustomProperties[PhotonRoomProperties.MAP_SEED] = seed;
    }
    #endregion
}