﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class OnConnectJoinRandomAndGenerateMap : ConnectAndJoinRandom {

    public override void OnPhotonRandomJoinFailed()
    {   
        Debug.Log("OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one. Calling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");

        PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = 20, CustomRoomProperties = PhotonGameController.instance.GetStartGameProperties() }, null);            
    }

    public new void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom() called by PUN. Now this client is in a room. From here on, your game would be running. For reference, all callbacks are listed in enum: PhotonNetworkingMessage");

        Room room = PhotonNetwork.room;
        if(room == null)
        {
            PhotonGameController.instance.error = "Player is not currently in a room. Try to connect again.";
        }
        else
        {
            PhotonGameController.instance.StartGameOnRoom(room);
        }
        
    }

    public override void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        PhotonGameController.instance.error = "Failed to connect to server. Please check your internet connection.";
        //Debug.LogError("SHOW ERROR MESSAGE HERE!!"); //Show message of No Internet Connection
    }
}
