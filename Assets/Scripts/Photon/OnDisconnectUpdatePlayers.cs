﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDisconnectUpdatePlayers : Photon.PunBehaviour {

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        base.OnPhotonPlayerConnected(newPlayer);
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        if (!PhotonNetwork.isMasterClient)
            return;
        
        if (otherPlayer.IsPlayerRescued())
        {
            PhotonPlayersController.instance.RemovePlayerHasBeenRescued(otherPlayer);
        }
        
        base.OnPhotonPlayerDisconnected(otherPlayer);
    }
}
