﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class OnMapGeneratedInstantiatePlayer : MonoBehaviour {

    public void OnMapGenerated()
    {
        Debug.Log("Map generated with success.");
        PhotonPlayersController.instance.NextGamePlayer();
        PhotonGameController.instance.StartGameTimer();
        PhotonGameController.instance.HideLoadingBackground();
        if (PhotonNetwork.room.IsGameModeHighscore())
            PhotonGameController.instance.EnableHighscoreTable();
    }

    
}
