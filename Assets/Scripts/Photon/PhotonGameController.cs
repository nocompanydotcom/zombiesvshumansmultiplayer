﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine.SceneManagement;

public class PhotonGameController : MonoBehaviour
{
	[SerializeField]
	private float gameTime = 5.0f;
	[SerializeField]
	private float rescueZoneTime = 1.0f;
	[SerializeField]
	private float highscoresTime = 0.2f;
	[SerializeField]
	private string sceneNameToEnterWhenAppPause = "MenuScene";

	[HideInInspector]
	public static PhotonGameController instance;

	private GameObject rescueZone;
	private GameObject rescueZoneLocator;
	private Timer timer;
	private GameObject gameLoadingBackground;
	private OnMapGeneratedInstantiatePlayer onMapGeneratedInstantiatePlayer;
	private Highscores highscores;
	private RescueZone rescueZoneController;
	private PhotonView m_PhotonView;

    public string error = null;

	void Awake()
	{
		if (instance == null)
			instance = this;

		else if (instance != this)
			Destroy(this);

		this.onMapGeneratedInstantiatePlayer = this.gameObject.GetComponent<OnMapGeneratedInstantiatePlayer>();
		if (this.onMapGeneratedInstantiatePlayer == null)
			Debug.LogError("No 'OnMapGeneratedInstantiatePlayer' script associated with the gameobject denominated by '" + this.gameObject.name + "'");

		this.m_PhotonView = this.GetComponent<PhotonView>();
	}

	private void Start()
	{
		GameObject timerObject = GameObject.FindGameObjectWithTag("Timer");
		if (timerObject == null)
			Debug.LogError("No GameObject with the following tag 'Timer'");
		else
		{
			this.timer = timerObject.GetComponent<Timer>();
			if (this.timer == null)
				Debug.LogError("No script denominated 'Timer' associated with GameObject with 'Timer' tag");
		}

		this.gameLoadingBackground = GameObject.FindGameObjectWithTag("LoadingGameBackground");
		if (this.gameLoadingBackground == null)
			Debug.LogError("No game loading background object found in the scene.");

		if(this.rescueZone == null)
		{
			this.rescueZone = GameObject.FindGameObjectWithTag("RescueZone");
			if (this.rescueZone == null)
				Debug.LogError("No GameObject with the following tag found 'RescueZone'");
		}

		if (this.rescueZoneLocator == null)
		{
			this.rescueZoneLocator = GameObject.FindGameObjectWithTag("RescueZoneLocator");
			if (this.rescueZoneLocator == null)
				Debug.LogError("No GameObject with the following tag found 'RescueZoneLocator'");
		}

		this.highscores = this.gameObject.GetComponent<Highscores>();
		if (this.highscores == null)
			Debug.LogError("No 'Highscores' script associated with the gameobject denominated by '" + this.gameObject.name + "'");

		this.rescueZoneController = this.rescueZone.GetComponent<RescueZone>();
		if (this.rescueZoneController == null)
			Debug.LogError("No 'RescueZone' script associated with the gameobject denominated by '" + this.gameObject.name + "'");
    }

    private void OnApplicationPause(bool pause)
	{
		if (pause && PhotonNetwork.connected)
		{
			PhotonNetwork.Disconnect();
			SceneManager.LoadScene(this.sceneNameToEnterWhenAppPause, LoadSceneMode.Single);
		}
	}

	//Executed by all players!!
	[PunRPC]
	public void GameOver()
	{
		GAME_MODE nextGameMode = PhotonNetwork.room.GetNextGameMode();
		GAME_MODE currentGameMode = PhotonNetwork.room.GetCurrentGameMode();
		if (currentGameMode == nextGameMode) //Repeated call
			return;

		currentGameMode = nextGameMode;

		PhotonNetwork.room.SetGameMode(nextGameMode);
		switch(currentGameMode)
		{
		case GAME_MODE.RESCUE_ZONE:
			this.EnableRescueZone();
			break;

		case GAME_MODE.HIGHSCORES:
			this.DisableRescueZone();
			this.EnableHighscoreTable();
			break;

		case GAME_MODE.GAME_OVER:
			this.DisableHighscoreTable();
			this.highscores.ResetPlayersScore();
			if(PhotonNetwork.player.IsMasterClient)
			{
				bool isHuman = Random.value > 0.5f;
				this.Restart(isHuman);
			}
			break;
		}
	}

	public void EnableHighscoreTable()
	{
		this.timer.SetTimer(this.highscoresTime);        
		this.highscores.Enable();
	}

	private void DisableHighscoreTable()
	{
		this.highscores.Disable();
	}

	[PunRPC]
	public void Restart(bool isHuman)
	{
		PhotonNetwork.room.SetGameMode(GAME_MODE.RESTART);
		PhotonPlayersController.instance.isNextTypeHuman = isHuman;
		MapController.instance.Clean();
		PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player);
		this.timer.ResetGameTime();
		this.StartGameOnRoom(PhotonNetwork.room);
		PhotonNetwork.room.SetGameMode(GAME_MODE.NORMAL);

        if (PhotonNetwork.player.IsMasterClient)
		{
			int numHumans = 0;
			int numZombies = 0;
			if (isHuman)
				numHumans++;
			else
				numZombies++;

			foreach (PhotonPlayer p in PhotonNetwork.playerList)
			{
				if (p.IsLocal)
					continue;

				if (numZombies <= numHumans)
				{
					isHuman = false;
					numZombies++;
				}
				else
				{
					isHuman = true;
					numHumans++;
				}

				this.m_PhotonView.RPC("Restart", p, isHuman);
			}
		}
	}

	public void StartGameOnRoom(Room room)
	{
		if(PhotonNetwork.player.IsMasterClient) //Only MasterClient can reset game properties
			this.ResetGameProperties();

		if (room.CustomProperties.ContainsKey(PhotonRoomProperties.MAP_SEED))
		{
			string mapSeed = room.CustomProperties[PhotonRoomProperties.MAP_SEED].ToString();
            this.GenerateMap(mapSeed);
			if (this.onMapGeneratedInstantiatePlayer != null)
				this.onMapGeneratedInstantiatePlayer.OnMapGenerated();
		}
		else
		{
			Debug.LogError("No seed specified.");
		}
	}

	private void GenerateMap(string seed)
	{
		MapController.instance.SetSeed(seed);
		MapController.instance.InitMap();
	}

	private void ResetGameProperties()
	{
        if (PhotonNetwork.room.TryGetCurrentGameMode() != GAME_MODE.RESTART)
            PhotonNetwork.room.SetGameMode(GAME_MODE.NORMAL);

		Hashtable roomProperties = this.GetStartGameProperties();
		PhotonNetwork.room.SetCustomProperties(roomProperties);
	}

	public void StartGameTimer()
	{
		if (this.timer == null)
			return;

		this.timer.enabled = true;
	}

	public void HideLoadingBackground()
	{
		this.gameLoadingBackground.SetActive(false);
	}

	public Hashtable GetStartGameProperties()
	{
		Hashtable roomProperties = new Hashtable();
		string mapSeed;
		if (PhotonNetwork.isMasterClient || PhotonNetwork.room == null) //If room == null means that the room it's still not created, so it means it's Starting a game and not restarting
			mapSeed = MapGenerator.NextSeed();
		else
			mapSeed = PhotonNetwork.room.CustomProperties[PhotonRoomProperties.MAP_SEED] as string;

		roomProperties.Add(PhotonRoomProperties.MAP_SEED, mapSeed);
		roomProperties.Add(PhotonRoomProperties.RESCUE_ZONE_ENABLED, false);
		roomProperties.Add(PhotonRoomProperties.NUM_RESCUED_PLAYERS, 0);
		roomProperties.Add(PhotonRoomProperties.GAME_TIME, this.gameTime);

		return roomProperties;
	}

	private void EnableRescueZone()
	{
		this.timer.SetTimer(this.rescueZoneTime);
		if (PhotonNetwork.player.IsMasterClient)
		{
			SpawnPointGenerator spawnPointGenerator = MapController.instance.getSpawnPointGenerator();
			Vector2 position = spawnPointGenerator.generateRescueZoneSpawnPoint();
			this.rescueZone.transform.position = MapController.instance.getRealPosition(position);
		}

		this.rescueZoneController.Enable();

		RescueZoneLocator rescueZoneLocatorScript = this.rescueZoneLocator.GetComponent<RescueZoneLocator>();
		rescueZoneLocatorScript.Enable();
	}

	private void DisableRescueZone()
	{
		this.rescueZoneController.Disable();
		RescueZoneLocator rescueZoneLocatorScript = this.rescueZoneLocator.GetComponent<RescueZoneLocator>();
		rescueZoneLocatorScript.Disable();
	}

	public PhotonView GetPhotonView()
	{
		return this.m_PhotonView;
	}
}
