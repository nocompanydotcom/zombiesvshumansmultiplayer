﻿using UnityEngine;
using System.Collections;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PhotonPlayersController : MonoBehaviour
{
	[SerializeField]
	private GameObject humanPrefab;   // set in inspector
	[SerializeField]
	private GameObject zombiePrefab;   // set in inspector

	public static int ANIM_TRANSFORM_TIME = 2, //In seconds
	INVALID_PLAYER_KILLS = -1;

	public static PhotonPlayersController instance;
	[HideInInspector]
	public PhotonCharacter character;

	public bool isNextTypeHuman; //Variable used only on the Restart game mode

	void Awake()
	{
		if (instance == null)
			instance = this;

		else if (instance != this)
			Destroy(this);
	}

	public void TransformPlayerWithDelay(PhotonCharacter character)
	{
		PhotonPlayersController.instance.character = character;
		Invoke("TransformPlayerNoArgs", ANIM_TRANSFORM_TIME);
	}

	private void TransformPlayerNoArgs()
	{
		PhotonPlayersController.instance.TransformPlayer(PhotonPlayersController.instance.character);
	}

	public void TransformPlayer(PhotonCharacter character)
	{
		GameObject prefab = (character.IsHuman() ? zombiePrefab : humanPrefab);
		Vector2 position = character.gameObject.transform.position;
		GameObject obj = PhotonNetwork.Instantiate(prefab.name, position, Quaternion.identity, 0);
		PhotonNetwork.Destroy(character.gameObject); //Destroy first
	}

	public GameObject NextGamePlayer()
	{
		//If this row is enable the items would not appear!!
		//PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player); //Destroy old player Objects if any

		bool isToCreateHuman = IsNextTypeOfCharacterHuman();
		Debug.Log("Instantiating player as human?? -> " + isToCreateHuman);
		////UpdatePhotonPlayerProperties(isToCreateHuman, PhotonPlayersController.INVALID_PLAYER_KILLS, false);
		return CreateGamePlayer(isToCreateHuman);
	}

	private GameObject CreateGamePlayer(bool isToCreateAHuman)
	{
		GameObject prefab;
		Vector2 spawnPos;
		SpawnPointGenerator spawnPointGenerator = MapController.instance.getSpawnPointGenerator();

		if (isToCreateAHuman)
		{
			prefab = this.GetHumanPrefab();
			spawnPos = spawnPointGenerator.generateHumanSpawnPoint();
		}
		else
		{
			prefab = this.GetZombiePrefab();
			spawnPos = spawnPointGenerator.generateZombieSpawnPoint();
		}

		return PhotonNetwork.Instantiate(prefab.name, MapController.instance.getRealPosition(spawnPos), Quaternion.identity, 0);
	}
	/*
     * Function used to determine if the next character is a Zombie or a Human.
     * NOTE:
     *  This function calls in the end the 'UpdateNumberOfPlayers' function to update the number of zombies and humans in the room.
    */
	private bool IsNextTypeOfCharacterHuman()
	{
		if (PhotonNetwork.room.GetCurrentGameMode() == GAME_MODE.RESTART)
			return this.isNextTypeHuman;

		TotalPlayers totalPlayers = this.GetTotalPlayersInRoom();
		bool isHuman;
		if (totalPlayers.GetNumZombies() == 0 || totalPlayers.GetNumZombies() <= totalPlayers.GetNumHumans()) //If true is a Zombie
		{
			isHuman = false;
		}
		else
		{
			isHuman = true;
		}

		return isHuman;
	}

	public void RemovePlayerHasBeenRescued(PhotonPlayer player)
	{
		PhotonNetwork.room.RemovePlayerHasRescued(player);
	}

	public void PlayerRescued(PhotonPlayer ownerOfPlayerRescued)
	{
		if (!PhotonNetwork.isMasterClient)
			return;

		if (ownerOfPlayerRescued.IsPlayerRescued())
        	ownerOfPlayerRescued.AddScore(50);
		
		int num_rescued_players = PhotonNetwork.room.AddPlayerHasRescued(ownerOfPlayerRescued);
		if (num_rescued_players >= this.GetTotalPlayersInRoom().GetNumHumans())
			PhotonGameController.instance.GetPhotonView().RPC("GameOver", PhotonTargets.All, null);
	}

	private TotalPlayers GetTotalPlayersInRoom()
	{
		TotalPlayers totalPlayers = new TotalPlayers();
		foreach (PhotonPlayer p in PhotonNetwork.playerList)
		{
			if (p.IsHuman())
			{
				totalPlayers.AddHuman();
			}
			else
			{
				totalPlayers.AddZombie();
			}
		}

		return totalPlayers;
	}

	/****************************************** GETTER's and SETTER's ******************************************/

	public GameObject GetZombiePrefab()
	{
		return this.zombiePrefab;
	}

	public GameObject GetHumanPrefab()
	{
		return this.humanPrefab;
	}
}
