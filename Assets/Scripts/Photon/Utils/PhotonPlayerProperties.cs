﻿

public static class PhotonPlayerProperties
{
    public const string PLAYER_TYPE = "PlayerType",
                        PLAYER_HP = "PlayerHP",
                        PLAYER_KILLS = "PlayerKills",
                        PLAYER_RESCUED = "PlayerRescued";
}
