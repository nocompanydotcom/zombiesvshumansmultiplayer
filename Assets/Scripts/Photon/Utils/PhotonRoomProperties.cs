﻿
public static class PhotonRoomProperties {

    public const string MAP_SEED = "MapSeed",
                        GAME_TIME = "GameTime",
                        RESCUE_ZONE_ENABLED = "RescueZoneEnabled",
                        NUM_RESCUED_PLAYERS = "NumOfRescuedPlayers",
                        GAME_MODE = "GameMode";
}
