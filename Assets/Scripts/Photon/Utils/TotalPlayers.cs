﻿using UnityEngine;
using System.Collections;

public class TotalPlayers
{
    private int numHumans;
    private int numZombies;

    public TotalPlayers()
    {
        this.numHumans = 0;
        this.numZombies = 0;
    }

    public TotalPlayers(int numHumans, int numZombies)
    {
        this.numHumans = numHumans;
        this.numZombies = numZombies;
    }

    public void AddHuman()
    {
        this.numHumans++;
    }

    public void AddZombie()
    {
        this.numZombies++;
    }

    public int GetNumHumans()
    {
        return this.numHumans;
    }

    public int GetNumZombies()
    {
        return this.numZombies;
    }

    public int GetTotal()
    {
        return this.numHumans + this.numZombies;
    }
}
