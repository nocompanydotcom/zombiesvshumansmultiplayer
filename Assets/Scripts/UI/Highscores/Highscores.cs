﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tacticsoft;

public class Highscores : MonoBehaviour, ITableViewDataSource {

    [SerializeField]
    private PlayerScoreTableViewCell m_cellPrefab;
    [SerializeField]
    private TableView m_tableView;
    [SerializeField]
    private GameObject m_tableViewParent;

    private List<PlayerScore> playersScores;

    #region HighscoresTableController

    public void Enable()
    {
        this.playersScores = this.GetPlayersScore();
        this.m_tableViewParent.gameObject.SetActive(true);
        this.m_tableView.dataSource = this;
    }

    public void Disable()
    {
        this.m_tableViewParent.gameObject.SetActive(false);
    }

    private List<PlayerScore> GetPlayersScore()
    {
        List<PlayerScore> playersScore = new List<PlayerScore>();
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            string name = player.NickName;
            int score = player.GetScore();
            bool isHuman = player.IsHuman();
            bool isLocalPlayer = player.IsLocal;

            PlayerScore playerScore = new PlayerScore(name, score, isHuman, isLocalPlayer);
            playersScore.Add(playerScore);
        }

        return GetSortedList(playersScore);
    }

    public void ResetPlayersScore()
    {
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            player.SetScore(0);
        }
    }

    private List<PlayerScore> GetSortedList(List<PlayerScore> playerScores)
    {
        playerScores.Sort((x, y) => y.GetScore().CompareTo(x.GetScore()));
        return playerScores;
    }

    #endregion
    #region ITableViewDataSource

    public TableViewCell GetCellForRowInTableView(TableView tableView, int row)
    {
        PlayerScoreTableViewCell cell = tableView.GetReusableCell(m_cellPrefab.reuseIdentifier) as PlayerScoreTableViewCell;
        if (cell == null)
        {
            cell = (PlayerScoreTableViewCell)GameObject.Instantiate(m_cellPrefab);
        }
        PlayerScore playerScore = this.playersScores[row];
        cell.SetPlayerName(playerScore.GetName(), row, playerScore.IsLocalPlayer());
        cell.SetScore(playerScore.GetScore());
        cell.SetIsHuman(playerScore.IsHuman());
        return cell;
    }

    public float GetHeightForRowInTableView(TableView tableView, int row)
    {
        return 50.0f;
    }

    public int GetNumberOfRowsForTableView(TableView tableView)
    {
        return (this.playersScores == null) ? 0 : playersScores.Count;
    }

    #endregion
}
