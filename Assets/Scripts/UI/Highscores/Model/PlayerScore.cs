﻿using UnityEngine;
using System.Collections;

public class PlayerScore
{
    private string name;
    private int score;
    private bool isHuman;
    private bool isLocalPlayer;

    public PlayerScore(string name, int score, bool isHuman, bool isLocalPlayer)
    {
        this.name = name;
        this.score = score;
        this.isHuman = isHuman;
        this.isLocalPlayer = isLocalPlayer;
    }

    public string GetName()
    {
        return this.name;
    }

    public int GetScore()
    {
        return this.score;
    }

    public bool IsHuman()
    {
        return this.isHuman;
    }

    public bool IsLocalPlayer()
    {
        return this.isLocalPlayer;
    }
}
