﻿using UnityEngine;
using System.Collections;
using Tacticsoft;
using UnityEngine.UI;

public class PlayerScoreTableViewCell : TableViewCell
{
    [SerializeField]
    private Text m_playerNameText;
    [SerializeField]
    private Text m_playerScoreText;
    [SerializeField]
    private Color humanCellColor;
    [SerializeField]
    private Color zombieCellColor;
    

    public void SetPlayerName(string playerName, int row, bool isLocalPlayer)
    {
        if (string.IsNullOrEmpty(playerName))
            playerName = "NO NAME " + row;

        if (isLocalPlayer)
            playerName += " (You)";

        this.m_playerNameText.text = playerName;
    }

    public void SetScore(int score)
    {
        this.m_playerScoreText.text = "" + score;
    }

    public void SetIsHuman(bool isHuman)
    {
        Image image = this.gameObject.GetComponent<Image>();
        if (image == null)
            Debug.LogError("No 'Image' script associated with this gameobject " + this.name);
        else
        {
            Color newColor = (isHuman ? this.humanCellColor : zombieCellColor);
            newColor.a = 0.4f;
            image.color = newColor;
        }
    }
}
