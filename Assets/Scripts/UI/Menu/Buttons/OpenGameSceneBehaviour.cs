﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OpenGameSceneBehaviour : OpenSceneBehaviour
{
    [SerializeField]
    private InputField playerNameInputField;

    public override void OnClick()
    {
        if (this.sceneToLoad == null)
        {
            Debug.LogWarning("No scene specified for loading...");
        }
        else
        {
            string playerName = this.playerNameInputField.text;
            if(string.IsNullOrEmpty(playerName))
            {
                Debug.LogError("You need to insert a PlayerName First!!");
            }
            else
            {
                PhotonNetwork.player.NickName = playerName;
                this.OpenScene(this.sceneToLoad);
            }
        }
    }
}
