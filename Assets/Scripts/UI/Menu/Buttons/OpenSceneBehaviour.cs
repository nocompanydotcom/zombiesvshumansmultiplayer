﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OpenSceneBehaviour : MonoBehaviour
{

    [SerializeField]
    protected string sceneToLoad;

    protected Button button;

    private void Start()
    {
        this.button = this.gameObject.GetComponent<Button>();
        if(button == null)
        {
            Debug.Log("No 'Button' script component associated with the following gameobject: " + this.gameObject.name);
        }
        else
        {
            button.onClick.AddListener(OnClick);
        }
    }

    public virtual void OnClick()
    {
        if (sceneToLoad == null)
        {
            Debug.LogWarning("No scene specified for loading...");
        }
        else
        {
            OpenScene(this.sceneToLoad);
        }
    }

    protected void OpenScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
