﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenRotationManager : MonoBehaviour {

    private void Awake()
    {
        DontDestroyOnLoad(this); //Keep it during all game
    }

    // Use this for initialization
    void Start () {
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;

        Screen.orientation = ScreenOrientation.AutoRotation;
    }
}
