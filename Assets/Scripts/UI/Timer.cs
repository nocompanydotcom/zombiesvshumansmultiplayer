﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class Timer : MonoBehaviour {
    
    private Text timeText;
    private float gameTime;
    private PhotonView m_PhotonView;

    private void Awake()
    {
        this.m_PhotonView = this.GetComponent<PhotonView>();
        this.timeText = GetComponent<Text>();
    }

    // Use this for initialization
    void Start () {
        this.ResetGameTime();
    }
	
	// Update is called once per frame
	void Update () {
        if (PhotonNetwork.room.IsGameOver())
        {
            timeText.text = "";
            this.enabled = false;
        }

        if (!PhotonNetwork.isMasterClient)
            return;
        
        this.gameTime -= Time.deltaTime;

        this.m_PhotonView.RPC("UpdateTimerUI", PhotonTargets.All, this.gameTime);
        if (gameTime <= 0)
        {
           this.m_PhotonView.RPC("GameOver", PhotonTargets.All);
        }
	}

    [PunRPC]
    public void UpdateTimerUI(float gameTime)
    {
        this.gameTime = gameTime;
        if (gameTime < 0)
            gameTime = 0;

        float minutes = Mathf.Floor(gameTime / 60),
              seconds = Mathf.Floor(gameTime % 60);

        timeText.text = minutes.ToString("00") + ":" + seconds.ToString("00"); //00:00 format
    }
    
    [PunRPC]
    public void GameOver()
    {
        PhotonGameController.instance.GameOver();
    }

    public void ResetGameTime()
    {
        if (!PhotonNetwork.isMasterClient)
            return;
        
        this.gameTime = Time.deltaTime + ((float)PhotonNetwork.room.CustomProperties[PhotonRoomProperties.GAME_TIME] * 60);
    }

    public void SetTimer(float gameTime)
    {
        if (!PhotonNetwork.player.IsMasterClient)
            return;

        this.gameTime = Time.deltaTime + (gameTime * 60);
    }
}
