﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightLineEquation {

    /**
     *  This function have been taken and updated from (previously called GetLine(...)): https://github.com/SebLague/Procedural-Cave-Generation/blob/master/Episode%2008/MapGenerator.cs
     * 
     * This function returns:
     *  Null if any of the points is Null or
     *  A list with points between the two points
     *  
     *  'maxWdith' and 'maxHeight' are exclusive
     */
    public static List<Vector2> getPointsBetween(Vector2 fromPoint, Vector2 toPoint, int maxWidth, int maxHeight, int thickness)
    {
        List<Vector2> vectors = new List<Vector2>();
        if (fromPoint == toPoint)
        {
            Debug.LogWarning("The two points passed to StraightLineEquation are the same");
            vectors.Add(fromPoint);
        }
        else if(thickness == 0)
        {
            Debug.LogWarning("Thickness for the connection beetween two Vector2 points is 0. No connection gonna be done.");
        }
        else
        {
            int x = (int)fromPoint.x;
            int y = (int)fromPoint.y;

            int dx = (int)(toPoint.x - fromPoint.x);
            int dy = (int)(toPoint.y - fromPoint.y);

            bool inverted = false;
            int step = System.Math.Sign(dx);
            int gradientStep = System.Math.Sign(dy);

            int longest = Mathf.Abs(dx);
            int shortest = Mathf.Abs(dy);

            if (longest < shortest)
            {
                inverted = true;
                longest = Mathf.Abs(dy);
                shortest = Mathf.Abs(dx);

                step = System.Math.Sign(dy);
                gradientStep = System.Math.Sign(dx);
            }

            int gradientAccumulation = longest / 2;
            for (int i = 0; i <= longest; i++)
            {
                vectors.Add(new Vector2(x, y));

                if (inverted)
                {
                    y += step;
                }
                else
                {
                    x += step;
                }

                gradientAccumulation += shortest;
                if (gradientAccumulation >= longest)
                {
                    if (inverted)
                    {
                        x += gradientStep;
                    }
                    else
                    {
                        y += gradientStep;
                    }
                    gradientAccumulation -= longest;
                }
            }
        }

        return StraightLineEquation.getScaledVectors(vectors, maxWidth, maxHeight, thickness);
    }
    /**
     * This function is used to create more points around a list of points 'vectors'.
     * Example:
     *      Foreach point on the list 'vectors' it will create more points around that one, like point of (1, 1) with thickness of 2 would generate:
     *          (-1, -1)
     *          (-1, 0)
     *          (-1, 1)
     *          (0, -1)
     *          (0, 0)
     *          (0, 1)
     *          (1, -1)
     *          (1, 0)
     *          (1, 1)
     */
    public static List<Vector2> getScaledVectors(List<Vector2> vectors, int maxWidth, int maxHeight, int thickness)
    {
        if (thickness <= 1)
            return vectors;
        else
        {
            List<Vector2> scaledVectors = new List<Vector2>();
            thickness -= 1;

            foreach (Vector2 v in vectors)
            {
                int xIndex = (int)v.x,
                    yIndex = (int)v.y,
                    startXIndex = xIndex - thickness,
                    startYIndex = yIndex - thickness,
                    endX = xIndex + thickness,
                    endY = yIndex + thickness;

                while (startXIndex < 0)
                {
                    startXIndex += 1;
                }

                while (startYIndex < 0)
                {
                    startYIndex += 1;
                }

                while (endX > maxWidth)
                {
                    endX -= 1;
                }

                while (endY > maxHeight)
                {
                    endY -= 1;
                }

                for (int x = startXIndex; x < endX; x++)
                {
                    for (int y = startYIndex; y < endY; y++)
                    {
                        Vector2 position = new Vector2(x, y);
                        scaledVectors.Add(position);
                    }
                }
            }

            return scaledVectors;
        }
    }
}
