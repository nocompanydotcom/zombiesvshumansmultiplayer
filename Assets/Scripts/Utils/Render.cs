﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Render {

    public static SpriteRenderer getSpriteRenderer(GameObject obj, Sprite sprite, string sortingLayer)
    {
        SpriteRenderer render = obj.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
        render.sortingLayerName = sortingLayer;
        render.sprite = sprite;

        return render;
    }

    public static void renderSprite(GameObject obj, Sprite sprite, string sortingLayer)
    {
        getSpriteRenderer(obj, sprite, sortingLayer);
    }

    public static Sprite getSpriteFromGameObject(GameObject obj)
    {
        SpriteRenderer render = obj.GetComponent<SpriteRenderer>();
        return render.sprite;
    }
}
