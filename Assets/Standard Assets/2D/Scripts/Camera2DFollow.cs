using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets._2D
{
    public class Camera2DFollow : MonoBehaviour
    {
        public Transform target;
        public float damping = 1;
        public float lookAheadFactor = 3;
        public float lookAheadReturnSpeed = 0.5f;
        public float lookAheadMoveThreshold = 0.1f;

        private float m_OffsetZ;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;

        private bool canUpdateView;
        private bool hasInitialized;

        // Use this for initialization
        private void Start()
        {
            if ("MultiplayerScene".Equals(SceneManager.GetSceneAt(0).name))
            {
                Init();
            }
        }


        // Update is called once per frame
        private void Update()
        {
            if(hasInitialized)
            {
                if (canUpdateView && target != null)
                {
                    UpdateView();
                }
            }
            else
            {
                if("MultiplayerScene".Equals(SceneManager.GetSceneAt(0).name))
                {
                    Init();
                }
            }
        }

        private void Init()
        {
            if (TryToStart())
            {
                StartFollowing();
            }

            this.hasInitialized = true;
        }

        private void UpdateView()
        {
            // only update lookahead pos if accelerating or changed direction
            float xMoveDelta = (target.position - m_LastTargetPosition).x;

            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

            if (updateLookAheadTarget)
            {
                m_LookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
            }
            else
            {
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);
            }

            Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward * m_OffsetZ;
            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);

            transform.position = newPos;

            m_LastTargetPosition = target.position;
        }

        public void StartFollowing()
        {
            m_LastTargetPosition = target.position;
            m_OffsetZ = (transform.position - target.position).z;
            transform.parent = null;
            canUpdateView = true;
        }

        private bool TryToStart()
        {
            if (target == null)
            {
                GameObject player = GameObject.Find("Player");
                if (player == null)
                    Debug.LogWarning("No Player tag object found");
                else
                {
                    target = player.transform;
                    return true;
                }
            }

            return false;
        }

        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
}
