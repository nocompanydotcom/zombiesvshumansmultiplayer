# Zombies vs Humans - Multiplayer

This repository contains the Assets folder from the Unity Project of the source code of the game *Zombies vs Humans - Multiplayer*. This game has been built for Android and iOS.
There are two gameplay modes: *Human* or *Zombie* (see images bellow).

![Game sample](GameSamples/Gameplay.gif)

![Gameplay views](GameSamples/GameplayViews.jpg)

## Game description

This is a 2D game that was built for players that love to test their skills in a challenging multiplayer environment, where the Humans try to survive until the last second and the Zombies try to extinct their opponents by contaminating them.

## Installation

* Open Unity and create a new project.
* Copy the Assets folder from these repository into the project directory.

## Project description

Inside the Assets folder you will find a set of folders, each one containing different parts of the source code. Those sub-folders are:

1. **Animation**: Contains all the animation clips.
2. **Animator**: Contains all the files related to the animators API of the Unity.
3. **Photon Unity Networking**: Multiplayer framework used.
4. **Plugins** 
5. **Prefabs**
6. **Scenes**: Contains all the scenes from the game.
7. **Scripts**: Contains all the explicit source of the game.
8. **Sounds**
9. **Sprites**
10. **Standard Assets**: Contains files from other frameworks used, such as multi-touch, etc.
11. **TSTableView**: Contains all the files related to the framework TSTableView, for creating tables in Unity.

### Most relevant files

1. **Assets/Scripts/Map/MapController.cs** - Script that can be attached to an object and is responsable to automatically generate 2D map (with a lot of available properties like width, height and fulfillment percentage) and also responsible for choosing the ammount of items and which kind of items to spawn on the map.
2. **Assets/Scripts/GameObjects/Player/PhotonCharacter.cs** - Script that is associated to any player of the game. A human has *Assets/Scripts/GameObjects/PhotonHuman.cs* script associated and a zombie has the *Assets/Scripts/GameObjects/PhotonZombie.cs* script. Nevertheless, both of them inherits from the *PhotonCharacter.cs* script.
3. **Assets/Scripts/GameObjects/Items/PhotonItem.cs** - Parent script that is associated to each item. In case that you want to create a new item, your object class must inherit from this script. Several items examples can be find inside the folder *Assets/Scripts/GameObjects/Items/Types*.
4. **Assets/Scripts/GameObjects/Bullet/PhotonMoveBullet.cs** and **Assets/Scripts/GameObjects/Bullet/PhotonShotBullet.cs** - Both files are needed to be associated to a prefab if you want to create a new bullet object.

In case of any doubt when creating new bullets, players, maps or items you can always check the existent prefabs inside the folder *Assets/Prefabs*.